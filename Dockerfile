# ----------------------------------------------------------------------------
# Copyright (C) 2018 Matilda Peak - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# ----------------------------------------------------------------------------

FROM registry.gitlab.com/matilda.peak/runtime-java:stable

# Labels
LABEL maintainer='Matilda Peak Limited <info@matildapeak.com>'

USER root

# Add chronicler user
RUN useradd --uid 6000 --shell /bin/bash --user-group --home-dir /opt/chronicler chronicler

# Create the project structure
RUN mkdir -p /opt/chronicler

COPY docker-entrypoint.sh /opt/chronicler
COPY docker-healthcheck.sh /opt/chronicler/
COPY build/libs/chronicler.jar /opt/chronicler/

RUN chmod 755 /opt/chronicler/docker-entrypoint.sh /opt/chronicler/docker-healthcheck.sh

RUN chown -R chronicler:chronicler /opt/chronicler

USER chronicler
WORKDIR /opt/chronicler
ENTRYPOINT ["./docker-entrypoint.sh"]
