#!/bin/bash

# -----------------------------------------------------------------------------
# Copyright (C) 2018 Matilda Peak - All Rights Reserved.
# Unauthorized copying of this file, via any medium is strictly prohibited.
# Proprietary and confidential.
# -----------------------------------------------------------------------------

# Is the application healthy
curl -s http://$HOSTNAME:8080/actuator/health | grep "^{\"status\":\"UP\""

# Return last exit code
exit
