/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler;

import com.matildapeak.chronicler.storage.queue.QueueConfiguration;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Checks that the configured actuators can be accessed and only with the expected credentials
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles({TestProfiles.DEFAULT_TEST_PROFILE, QueueConfiguration.QUEUE_PROFILE})
@DirtiesContext
public class SwaggerIT
{

    @Test
    public void anonymousUserCanAccessAPI() throws Exception
    {
        mvc.perform(get("/v2/api-docs"))
           .andExpect(status().isOk());
    }

    @Test
    public void anonymousUserCanAccessUI() throws Exception
    {
        mvc.perform(get("/swagger-ui.html"))
           .andExpect(status().isOk());
    }

    @Test
    public void apiHasExpectedDefinitions() throws Exception
    {
        mvc.perform(get("/v2/api-docs"))
           .andExpect(status().isOk())
           .andExpect(jsonPath("$.info.title").value(Matchers.equalToIgnoringCase("Chronicler REST API")))
           .andExpect(jsonPath("$.paths./inbound/{customer}/{domain}/{group}/{source}/{type}").exists())
           .andExpect(jsonPath("$.paths./outbound").exists())
           .andExpect(jsonPath("$.paths./outbound/{channelId}").exists());
    }

    @Autowired
    private MockMvc mvc;
}
