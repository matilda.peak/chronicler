/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.inbound;

import com.google.common.collect.ImmutableMap;
import com.matildapeak.chronicler.SecurityConfiguration;
import matildapeak.chronicler.EventMessageOuterClass.EventMessage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * In this test, the full Spring application context is started, but without the server, narrowed down
 * to the web layer exposed by a single controller.
 */
@RunWith(SpringRunner.class)
@WebMvcTest({InboundController.class, SecurityConfiguration.class})
public class InboundControllerIT
{
    @Test
    public void push() throws Exception
    {
        // given
        given(eventMessageFactory.make(
                eq("customer"),
                eq("domain"),
                eq("group"),
                eq("source"),
                eq("type"),
                eq(ImmutableMap.of("key-1", "value-1", "key-2", "value-2")),
                any())).willReturn(EventMessage.newBuilder()
                                               .setUuId("customer-1")
                                               .build());

        // when
        final MvcResult mvcResult = this.mvc.perform(
                post("/inbound/customer/domain/group/source/type")
                        .param("key-1", "value-1")
                        .param("key-2", "value-2")
                        .characterEncoding("UTF-8")
                        .content(new byte[]{0x61, 0x62, 0x63}))
                                            .andExpect(request().asyncStarted())
                                            .andExpect(request().asyncResult(notNullValue()))
                                            .andReturn();

        // wait for the async thread to process the task on the server side
        mvc.perform(asyncDispatch(mvcResult))
           .andExpect(status().is2xxSuccessful()) // Should be .isCreated() but test fails
           .andExpect(content().string("customer-1"));

        // then
        then(inboundService).should().push(any(EventMessage.class));
        then(eventMessageFactory).should(times(1)).make(eq("customer"),
                                                        eq("domain"),
                                                        eq("group"),
                                                        eq("source"),
                                                        eq("type"),
                                                        eq(ImmutableMap.of("key-1", "value-1", "key-2", "value-2")),
                                                        any());
    }

    @Autowired
    private MockMvc mvc;

    @MockBean
    private InboundService inboundService;

    @MockBean
    private EventMessageFactory eventMessageFactory;
}
