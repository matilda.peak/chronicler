/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.inbound;

import com.google.common.collect.ImmutableMap;
import com.matildapeak.chronicler.util.SequenceIdGenerator;
import matildapeak.chronicler.EventMessageOuterClass.EventMessage;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

public class EventMessageFactoryTest
{
    @Before
    public void initMocks()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void make()
    {
        // given
        given(eventIdGenerator.generate()).willReturn("ABC-1");

        final Map<String, String> header = ImmutableMap.of("key-1", "value-1", "key-2", "value-2");
        final byte[] payload = new byte[]{0x01, 0x02, 0x03};

        // when
        final EventMessage eventMessage = factory.make(
                "customer",
                "domain",
                "group",
                "source",
                "type",
                header,
                Optional.of(payload));

        // then
        assertThat(eventMessage.getUuId()).isEqualTo("ABC-1");
        assertThat(eventMessage.getCustomerId()).isEqualTo("customer");
        assertThat(eventMessage.getDomainId()).isEqualTo("domain");
        assertThat(eventMessage.getGroupId()).isEqualTo("group");
        assertThat(eventMessage.getSourceId()).isEqualTo("source");
        assertThat(eventMessage.getType()).isEqualTo("type");
        assertThat(eventMessage.getHeaderMap()).isEqualTo(header);
        assertThat(eventMessage.getPayload().toByteArray()).containsExactly(payload);

        then(eventIdGenerator).should().generate();
    }

    @Test
    public void makeWithNoPayload()
    {
        // given
        given(eventIdGenerator.generate()).willReturn("ABC-1");

        final Map<String, String> header = ImmutableMap.of("key-1", "value-1", "key-2", "value-2");

        // when
        final EventMessage eventMessage = factory.make(
                "customer",
                "domain",
                "group",
                "source",
                "type",
                header,
                Optional.empty());

        // then
        assertThat(eventMessage.getUuId()).isEqualTo("ABC-1");
        assertThat(eventMessage.getCustomerId()).isEqualTo("customer");
        assertThat(eventMessage.getDomainId()).isEqualTo("domain");
        assertThat(eventMessage.getGroupId()).isEqualTo("group");
        assertThat(eventMessage.getSourceId()).isEqualTo("source");
        assertThat(eventMessage.getType()).isEqualTo("type");
        assertThat(eventMessage.getHeaderMap()).isEqualTo(header);
        assertThat(eventMessage.getPayload().toByteArray()).isEmpty();

        then(eventIdGenerator).should().generate();
    }

    @Test
    public void makeWithNullPayload()
    {
        // given
        given(eventIdGenerator.generate()).willReturn("ABC-1");

        final Map<String, String> header = ImmutableMap.of("key-1", "value-1", "key-2", "value-2");

        // when
        final EventMessage eventMessage = factory.make(
                "customer",
                "domain",
                "group",
                "source",
                "type",
                header,
                Optional.ofNullable(null));

        // then
        assertThat(eventMessage.getUuId()).isEqualTo("ABC-1");
        assertThat(eventMessage.getCustomerId()).isEqualTo("customer");
        assertThat(eventMessage.getDomainId()).isEqualTo("domain");
        assertThat(eventMessage.getGroupId()).isEqualTo("group");
        assertThat(eventMessage.getSourceId()).isEqualTo("source");
        assertThat(eventMessage.getType()).isEqualTo("type");
        assertThat(eventMessage.getHeaderMap()).isEqualTo(header);
        assertThat(eventMessage.getPayload().toByteArray()).isEmpty();

        then(eventIdGenerator).should().generate();
    }

    @Test
    public void makeWithEmptyHeader()
    {
        // given
        given(eventIdGenerator.generate()).willReturn("ABC-1");

        final Map<String, String> header = ImmutableMap.of();

        final byte[] payload = new byte[]{0x01, 0x02, 0x03};

        // when
        final EventMessage eventMessage = factory.make(
                "customer",
                "domain",
                "group",
                "source",
                "type",
                header,
                Optional.of(payload));

        // then
        assertThat(eventMessage.getUuId()).isEqualTo("ABC-1");
        assertThat(eventMessage.getCustomerId()).isEqualTo("customer");
        assertThat(eventMessage.getDomainId()).isEqualTo("domain");
        assertThat(eventMessage.getGroupId()).isEqualTo("group");
        assertThat(eventMessage.getSourceId()).isEqualTo("source");
        assertThat(eventMessage.getType()).isEqualTo("type");
        assertThat(eventMessage.getHeaderMap()).isEmpty();
        assertThat(eventMessage.getPayload().toByteArray()).containsExactly(payload);

        then(eventIdGenerator).should().generate();
    }

    @Mock
    private SequenceIdGenerator eventIdGenerator;

    @InjectMocks
    private EventMessageFactory factory;
}