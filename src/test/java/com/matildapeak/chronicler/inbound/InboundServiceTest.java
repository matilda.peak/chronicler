/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.inbound;

import com.matildapeak.chronicler.storage.EventLogWriter;
import matildapeak.chronicler.EventMessageOuterClass.EventMessage;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.same;
import static org.mockito.BDDMockito.then;

public class InboundServiceTest
{
    @Before
    public void initMocks()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void push()
    {
        final EventMessage eventMessage = EventMessage.getDefaultInstance();

        inboundService.push(eventMessage);

        then(writer).should().append(same(eventMessage));
    }

    @Mock
    private EventLogWriter writer;

    @InjectMocks
    private InboundService inboundService;
}