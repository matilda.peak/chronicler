/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.util;

import com.google.common.io.BaseEncoding;
import org.junit.Test;

import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

public class UUIDGeneratorTest
{
    @Test
    public void makeID()
    {
        IntStream.range(0, 1000)
                 .mapToObj(value -> uuidGenerator.makeId())
                 .forEach(id ->
                          {
                              // The id is a valid base64 string
                              final byte[] bytes = BaseEncoding.base64Url().omitPadding().decode(id);

                              assertThat(id.length()).isEqualTo(22);                // 22 characters
                              assertThat(bytes.length).isEqualTo(16);               // 16 bytes

                              assertThat((bytes[6] & 0xF0) >>> 4).isEqualTo(0x04); // version 4
                          });
    }

    private final UUIDGenerator uuidGenerator = new UUIDGenerator();
}