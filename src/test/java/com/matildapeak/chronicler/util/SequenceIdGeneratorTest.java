/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.util;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SequenceIdGeneratorTest
{

    @Test
    public void generate()
    {
        final SequenceIdGenerator sequenceIdGenerator = new SequenceIdGenerator("prefix");

        assertThat(sequenceIdGenerator.generate()).isEqualTo("prefix" + '1');
        assertThat(sequenceIdGenerator.generate()).isEqualTo("prefix" + '2');
        assertThat(sequenceIdGenerator.generate()).isEqualTo("prefix" + '3');
        assertThat(sequenceIdGenerator.generate()).isEqualTo("prefix" + '4');
        assertThat(sequenceIdGenerator.generate()).isEqualTo("prefix" + '5');
        assertThat(sequenceIdGenerator.generate()).isEqualTo("prefix" + '6');
        assertThat(sequenceIdGenerator.generate()).isEqualTo("prefix" + '7');
        assertThat(sequenceIdGenerator.generate()).isEqualTo("prefix" + '8');
    }
}