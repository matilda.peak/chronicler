/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.outbound;

import com.matildapeak.chronicler.storage.CancelledSupplierException;
import io.micrometer.core.instrument.Timer;
import matildapeak.chronicler.EventMessageOuterClass.EventMessage;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.BDDMockito.willAnswer;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

@SuppressWarnings("unchecked")
public class EmitterTaskTest
{
    @Before
    public void initMocks()
    {
        MockitoAnnotations.initMocks(this);
        emitterTask.timer = timer; // Not injected via constructor
    }

    @Test
    public void call() throws Exception
    {
        given(supplier.get())
                .willReturn(EventMessage.getDefaultInstance())
                .willReturn(EventMessage.getDefaultInstance())
                .willThrow(new CancelledSupplierException("Done"));

        willAnswer(invocation -> // Bypass the timer
                   {
                       invocation.<Runnable>getArgument(0).run();
                       return null;
                   })
                .given(timer).record(any(Runnable.class));

        emitterTask.call();

        then(supplier).should(times(3)).get();
        then(session).should(times(2)).sendMessage(any(BinaryMessage.class));
        then(session).should().close();
        then(timer).should(times(2)).record(any(Runnable.class));
    }

    @Test
    public void supplierRuntimeExceptionsExceptWakeUpArePassedOnAndSessionAlwaysCloses() throws Exception
    {
        given(supplier.get()).willThrow(new IllegalArgumentException("Computer says no"));

        final Throwable thrown = catchThrowable(() -> emitterTask.call());

        assertThat(thrown)
                .isNotNull()
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("Computer says no");
        then(session).should().close();
    }

    @Test
    public void wrappedInterruptedExceptionFromSupplierIsUnwrappedAndRethrownAndSessionAlwaysCloses() throws Exception
    {
        given(supplier.get())
                .willThrow(new IllegalStateException("Computer says no", new InterruptedException("Interrupted")));

        final Throwable thrown = catchThrowable(() -> emitterTask.call());

        assertThat(thrown)
                .isNotNull()
                .isInstanceOf(InterruptedException.class)
                .hasMessageContaining("Interrupted");
        then(session).should().close();
    }

    @Test
    public void exceptionsWhenClosingSessionAreSuppressed() throws Exception
    {
        given(supplier.get()).willThrow(new CancelledSupplierException("Done"));
        willThrow(new IOException("Computer says no")).given(session).close();

        emitterTask.call();

        then(supplier).should(times(1)).get();
        then(session).should(never()).sendMessage(any(BinaryMessage.class));
        then(session).should().close();
        then(timer).shouldHaveZeroInteractions();
    }

    @Test
    public void cancel()
    {
        final Function<Boolean, Boolean> taskCanceller = mock(Function.class);

        emitterTask.cancel(taskCanceller);

        then(supplier).should().cancel(taskCanceller);
    }

    @Mock
    private EventMessageSupplier supplier;

    @Mock
    private WebSocketSession session;

    @Mock
    private Timer timer;

    @InjectMocks
    private EmitterTask emitterTask;
}