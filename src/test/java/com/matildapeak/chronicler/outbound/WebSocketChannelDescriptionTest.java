/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.outbound;

import org.junit.Test;
import org.springframework.web.util.UriComponentsBuilder;

import static org.assertj.core.api.Assertions.assertThat;

public class WebSocketChannelDescriptionTest
{
    @Test
    public void constructor()
    {
        // given
        WebSocketChannelDescription.ID_GENERATOR.set(0L); // Reset for test
        final UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString("http://localhost/outbound");

        // when
        final WebSocketChannelDescription description = new WebSocketChannelDescription(uriComponentsBuilder);

        // then
        assertThat(description.getId()).isEqualTo(1L);
        assertThat(description.getUri())
                .hasScheme("ws")
                .hasHost("localhost")
                .hasPath("/channel/1");

    }
}