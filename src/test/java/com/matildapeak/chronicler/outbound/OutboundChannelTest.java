/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.outbound;

import com.matildapeak.chronicler.domain.SubscriptionCriteria;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.web.socket.WebSocketSession;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

public class OutboundChannelTest
{
    @Before
    public void initMocks()
    {
        MockitoAnnotations.initMocks(this);
        outboundChannel.outboundExecutorService = outboundExecutorService;
        outboundChannel.beanFactory = beanFactory;
    }

    @Test
    public void open()
    {
        // given
        final EventMessageSupplier eventMessageSupplier = mock(EventMessageSupplier.class);
        final EmitterTask emitterTask = mock(EmitterTask.class);
        final WebSocketSession webSocketSession = mock(WebSocketSession.class);

        given(beanFactory.getBean(EventMessageSupplier.class, subscriptionCriteria)).willReturn(eventMessageSupplier);
        given(beanFactory.getBean(EmitterTask.class, eventMessageSupplier, webSocketSession, "1")).willReturn(emitterTask);

        // when
        outboundChannel.open(webSocketSession, "1");

        // then
        then(beanFactory).should().getBean(EventMessageSupplier.class, subscriptionCriteria);
        then(beanFactory).should().getBean(EmitterTask.class, eventMessageSupplier, webSocketSession, "1");
        then(outboundExecutorService).should().submit(emitterTask);
    }

    @Test
    public void close()
    {
        outboundChannel.emitterTask = emitterTask;

        outboundChannel.close();

        then(emitterTask).should().cancel(false);
        assertThat(outboundChannel.emitterTask).isNull();
    }

    @Mock
    private BeanFactory beanFactory;

    @Mock
    private ExecutorService outboundExecutorService;

    @Mock
    private SubscriptionCriteria subscriptionCriteria;

    @Mock
    private Future<Void> emitterTask;

    @InjectMocks
    private OutboundChannel outboundChannel;
}