/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.outbound;

import org.junit.Test;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.socket.WebSocketHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

@SuppressWarnings("unchecked")
public class UriTemplateHandshakeInterceptorTest
{
    @Test
    public void beforeHandshake()
    {
        // given
        final ServletServerHttpRequest request = mock(ServletServerHttpRequest.class);
        final ServerHttpResponse response = mock(ServerHttpResponse.class);
        final WebSocketHandler wsHandler = mock(WebSocketHandler.class);
        final Map<String, Object> attributes = mock(Map.class);

        final HttpServletRequest origRequest = mock(HttpServletRequest.class);
        given(request.getServletRequest()).willReturn(origRequest);

        final Map<String, String> uriTemplateVariables = mock(Map.class);
        given(origRequest.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE)).willReturn(uriTemplateVariables);

        // when
        interceptor.beforeHandshake(request, response, wsHandler, attributes);

        // then
        then(request).should().getServletRequest();
        // Attribute that contains the URI templates map, mapping variable names to values
        then(origRequest).should().getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        then(attributes).should().putAll(uriTemplateVariables);
    }

    private final UriTemplateHandshakeInterceptor interceptor = new UriTemplateHandshakeInterceptor();
}