/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.outbound;

import com.matildapeak.chronicler.SecurityConfiguration;
import com.matildapeak.chronicler.domain.SubscriptionCriteria;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest({OutboundController.class, SecurityConfiguration.class})
public class OutboundControllerIT
{

    @Test
    public void open() throws Exception
    {
        // given
        final SubscriptionCriteria subscriptionCriteria = mock(SubscriptionCriteria.class);
        given(subscriptionCriteriaFactory.make("the criteria")).willReturn(subscriptionCriteria);

        final WebSocketChannelDescription webSocketChannelDescription
                = new WebSocketChannelDescription(101L, URI.create("ws://localhost"), "V1");

        given(eventService.prepareChannel(same(subscriptionCriteria),
                                          any(UriComponentsBuilder.class))).willReturn(webSocketChannelDescription);

        // when
        this.mockMvc.perform(post("/outbound")
                                     .characterEncoding("UTF-8")
                                     .contentType(MediaType.TEXT_PLAIN)
                                     .content("the criteria"))
                    .andExpect(status().isCreated())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                    .andExpect(jsonPath("$.id").value(101L))
                    .andExpect(jsonPath("$.uri").value("ws://localhost"))
                    .andExpect(jsonPath("$.version").value("V1"))
                    .andExpect(header().string("Location", "http://localhost/outbound/101"))
                    .andExpect(redirectedUrl("http://localhost/outbound/101"))
                    .andDo(print());

        // then
        then(subscriptionCriteriaFactory).should().make("the criteria");
        then(eventService).should().prepareChannel(same(subscriptionCriteria), any(UriComponentsBuilder.class));
    }

    @Test
    public void close() throws Exception
    {
        this.mockMvc.perform(delete("/outbound/101"))
                    .andExpect(status().isOk())
                    .andExpect(content().string("101"));

        then(eventService).should().closeChannel(101L);
    }

    @Test
    public void closeFailsIfChannelDoesNotExist() throws Exception
    {
        willThrow(new IllegalArgumentException("Channel 101 not found")).given(eventService).closeChannel(101L);

        this.mockMvc.perform(delete("/outbound/101"))
                    .andExpect(status().isNotFound())
                    .andExpect(content().string("101"));

        then(eventService).should().closeChannel(101L);
    }

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SubscriptionCriteriaFactory subscriptionCriteriaFactory;

    @MockBean
    private OutboundService eventService;
}