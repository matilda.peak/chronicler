/*
 * Copyright (c) 2018 Matilda Peak - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.outbound;

import com.matildapeak.chronicler.domain.SubscriptionCriteria;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.MalformedURLException;
import java.util.Map;

import static com.matildapeak.chronicler.outbound.OutboundController.CHANNEL_ID_PATH_PARAMETER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;

/**
 * Unit test for the {@link OutboundService}
 * <p>
 * Created by Alan Christie on 07/12/2017.
 */
@SuppressWarnings("unchecked")
public class OutboundServiceTest
{
    @Before
    public void initMocks()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void prepareChannel() throws MalformedURLException
    {
        // given
        final SubscriptionCriteria criteria = mock(SubscriptionCriteria.class);
        final UriComponentsBuilder uriComponentsBuilder = mock(UriComponentsBuilder.class);
        final WebSocketChannelDescription webSocketChannelDescription = mock(WebSocketChannelDescription.class);
        final OutboundChannel outboundChannel = mock(OutboundChannel.class);

        given(webSocketChannelDescription.getId()).willReturn(1001L);
        given(beanFactory.getBean(OutboundChannel.class, criteria)).willReturn(outboundChannel);
        willReturn(webSocketChannelDescription).given(service).makeWebSocketChannelDescription(uriComponentsBuilder);

        // when
        final WebSocketChannelDescription result = service.prepareChannel(criteria, uriComponentsBuilder);

        // then
        assertThat(result).isSameAs(webSocketChannelDescription);

        then(service).should().makeWebSocketChannelDescription(uriComponentsBuilder);
        then(beanFactory).should().getBean(OutboundChannel.class, criteria);
        then(webSocketChannelDescription).should().getId();
        then(channels).should().put(1001L, outboundChannel);
    }

    @Test
    public void openChannel()
    {
        // given
        final Map<String, Object> webSocketAttributes = mock(Map.class);
        given(webSocketAttributes.get(CHANNEL_ID_PATH_PARAMETER)).willReturn("1001");

        final WebSocketSession webSocketSession = mock(WebSocketSession.class);
        given(webSocketSession.getAttributes()).willReturn(webSocketAttributes);

        final OutboundChannel outboundChannel = mock(OutboundChannel.class);
        given(channels.get(1001L)).willReturn(outboundChannel);

        // when
        service.openChannel(webSocketSession);

        // then
        then(webSocketSession).should().getAttributes();
        then(webSocketAttributes).should().get(CHANNEL_ID_PATH_PARAMETER);
        then(channels).should().get(1001L);
        then(outboundChannel).should().open(webSocketSession, "1001");

        then(channels).shouldHaveNoMoreInteractions();
        then(webSocketSession).shouldHaveNoMoreInteractions();
    }

    @Test
    public void openChannelWhenIdAttributeNotInSession()
    {
        // given
        final Map<String, Object> webSocketAttributes = mock(Map.class);

        final WebSocketSession webSocketSession = mock(WebSocketSession.class);
        given(webSocketSession.getAttributes()).willReturn(webSocketAttributes);

        // when
        final Throwable thrown = catchThrowable(() -> service.openChannel(webSocketSession));

        // then
        assertThat(thrown).isInstanceOf(IllegalStateException.class)
                          .hasMessage("WebSocketSession is missing the channel ID");

        then(webSocketSession).should().getAttributes();
        then(webSocketAttributes).should().get(CHANNEL_ID_PATH_PARAMETER);

        then(channels).shouldHaveNoMoreInteractions();
        then(webSocketSession).shouldHaveNoMoreInteractions();
    }

    @Test
    public void openChannelWhenIdNotInRecords()
    {
        // given
        final Map<String, Object> webSocketAttributes = mock(Map.class);
        given(webSocketAttributes.get(CHANNEL_ID_PATH_PARAMETER)).willReturn("1001");

        final WebSocketSession webSocketSession = mock(WebSocketSession.class);
        given(webSocketSession.getAttributes()).willReturn(webSocketAttributes);

        // when
        final Throwable thrown = catchThrowable(() -> service.openChannel(webSocketSession));

        // then
        assertThat(thrown).isInstanceOf(IllegalArgumentException.class)
                          .hasMessage("No channel found for id = 1001");

        then(webSocketSession).should().getAttributes();
        then(webSocketAttributes).should().get(CHANNEL_ID_PATH_PARAMETER);
        then(channels).should().get(1001L);

        then(channels).shouldHaveNoMoreInteractions();
        then(webSocketSession).shouldHaveNoMoreInteractions();
    }

    @Test
    public void closeChannelWithWebSocketSession()
    {
        // given
        final Map<String, Object> webSocketAttributes = mock(Map.class);
        given(webSocketAttributes.get(CHANNEL_ID_PATH_PARAMETER)).willReturn("1001");

        final WebSocketSession webSocketSession = mock(WebSocketSession.class);
        given(webSocketSession.getAttributes()).willReturn(webSocketAttributes);

        final OutboundChannel outboundChannel = mock(OutboundChannel.class);
        given(channels.remove(1001L)).willReturn(outboundChannel);

        // when
        service.closeChannel(webSocketSession);

        // then
        then(webSocketSession).should().getAttributes();
        then(webSocketAttributes).should().get(CHANNEL_ID_PATH_PARAMETER);
        then(channels).should().remove(1001L);
        then(outboundChannel).should().close();

        then(channels).shouldHaveNoMoreInteractions();
        then(webSocketSession).shouldHaveNoMoreInteractions();
    }

    @Test
    public void closeChannelWithWebSocketSessionWhenIdAttributeNotInSession()
    {
        // given
        final Map<String, Object> webSocketAttributes = mock(Map.class);

        final WebSocketSession webSocketSession = mock(WebSocketSession.class);
        given(webSocketSession.getAttributes()).willReturn(webSocketAttributes);

        // when
        final Throwable thrown = catchThrowable(() -> service.closeChannel(webSocketSession));

        // then
        assertThat(thrown).isInstanceOf(IllegalStateException.class)
                          .hasMessage("WebSocketSession is missing the channel ID");

        then(webSocketSession).should().getAttributes();
        then(webSocketAttributes).should().get(CHANNEL_ID_PATH_PARAMETER);

        then(channels).shouldHaveNoMoreInteractions();
        then(webSocketSession).shouldHaveNoMoreInteractions();
    }

    @Test
    public void closeChannelWithWebSocketSessionWhenIdNotInRecords()
    {
        // given
        final Map<String, Object> webSocketAttributes = mock(Map.class);
        given(webSocketAttributes.get(CHANNEL_ID_PATH_PARAMETER)).willReturn("1001");

        final WebSocketSession webSocketSession = mock(WebSocketSession.class);
        given(webSocketSession.getAttributes()).willReturn(webSocketAttributes);

        // when
        final Throwable thrown = catchThrowable(() -> service.closeChannel(webSocketSession));

        // then
        assertThat(thrown).isInstanceOf(IllegalArgumentException.class)
                          .hasMessage("No channel found for id = 1001");

        then(webSocketSession).should().getAttributes();
        then(webSocketAttributes).should().get(CHANNEL_ID_PATH_PARAMETER);
        then(channels).should().remove(1001L);
        then(channels).shouldHaveZeroInteractions();
        then(webSocketSession).shouldHaveNoMoreInteractions();
    }

    @Test
    public void closeChannelWithChannelId()
    {
        // given
        final OutboundChannel outboundChannel = mock(OutboundChannel.class);
        given(channels.remove(1001L)).willReturn(outboundChannel);

        // when
        service.closeChannel(1001L);

        // then
        then(channels).should().remove(1001L);
        then(outboundChannel).should().close();
        then(channels).shouldHaveNoMoreInteractions();
    }

    @Test
    public void closeChannelWithChannelIdWhenIdNotInRecords()
    {
        // given
        // when
        final Throwable thrown = catchThrowable(() -> service.closeChannel(1001L));

        // then
        assertThat(thrown).isInstanceOf(IllegalArgumentException.class)
                          .hasMessage("No channel found for id = 1001");

        then(channels).should().remove(1001L);
        then(channels).shouldHaveNoMoreInteractions();
    }


    @Mock
    private Map<Long, OutboundChannel> channels;

    @Mock
    private BeanFactory beanFactory;

    @InjectMocks
    @Spy
    private OutboundService service;
}