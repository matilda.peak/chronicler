/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.outbound;

import com.google.common.eventbus.EventBus;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.BDDMockito.then;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.mock;

public class OutboundWebSocketHandlerTest
{
    @Before
    public void initMocks()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void channelIsOpenedAfterConnectionEstablished() throws IllegalStateException, IllegalArgumentException
    {
        final WebSocketSession session = mock(WebSocketSession.class);

        handler.afterConnectionEstablished(session);

        then(outboundService).should().openChannel(session);
        // TODO: should create an event
    }

    @Test
    public void handlerPassesOnServiceErrorsWhenOpeningChannel()
    {
        final WebSocketSession session = mock(WebSocketSession.class);
        willThrow(new IllegalArgumentException("Subscription already opened"))
                .given(outboundService).openChannel(session);

        final Throwable thrown = catchThrowable(() -> handler.afterConnectionEstablished(session));

        assertThat(thrown)
                .isNotNull()
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("Subscription already opened");
        then(outboundService).should().openChannel(session);
    }

    @Test
    public void webSocketSessionIsClosedWithErrorIfTransportErrorIsDetected() throws Exception
    {
        final WebSocketSession session = mock(WebSocketSession.class);
        final Throwable cause = mock(Throwable.class);

        handler.handleTransportError(session, cause);

        then(session).should().close(CloseStatus.SERVER_ERROR);
        then(outboundService).shouldHaveZeroInteractions();

        // TODO: raise internal event
//        ArgumentCaptor<ExceptionEvent> internalEvent = ArgumentCaptor.forClass(ExceptionEvent.class);
//        then(internalEventBus).should().post(internalEvent.capture());
//        assertThat(internalEvent.getValue().getCause()).isSameAs(cause);
//        assertThat(internalEvent.getValue().getReason()).isEqualTo("TODO");
    }

    @Test
    public void channelIsClosedAfterConnectionClosed()
    {
        final WebSocketSession session = mock(WebSocketSession.class);

        handler.afterConnectionClosed(session, CloseStatus.NORMAL);

        then(outboundService).should().closeChannel(session);
        // TODO: should create an event
    }

    @Test
    public void handlerSuppressesServiceErrorsWhenClosingChannel()
    {
        final WebSocketSession session = mock(WebSocketSession.class);
        willThrow(new IllegalArgumentException("Subscription already opened"))
                .given(outboundService).closeChannel(session);

        handler.afterConnectionClosed(session, CloseStatus.NORMAL);

        then(outboundService).should().closeChannel(session);
        // TODO: should create an event
    }

    @Mock
    private OutboundService outboundService;

    @Mock
    private EventBus internalEventBus;

    @InjectMocks
    private OutboundWebSocketHandler handler;
}