/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.storage.kafka;

import com.google.common.collect.ImmutableMap;
import com.google.protobuf.ByteString;
import com.matildapeak.chronicler.ChroniclerConfiguration;
import com.matildapeak.chronicler.TestProfiles;
import com.matildapeak.chronicler.domain.SubscriptionCriteria;
import matildapeak.chronicler.EventCategoryEnumOuterClass.EventCategoryEnum;
import matildapeak.chronicler.EventMessageOuterClass.EventMessage;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.test.rule.KafkaEmbedded;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes =
        {
                ChroniclerConfiguration.class,
                KafkaConfiguration.class,
                KafkaSupplier.class,
                KafkaWriter.class,
                KafkaSupplier.class
        })
@ActiveProfiles({KafkaConfiguration.KAFKA_PROFILE, TestProfiles.DEFAULT_TEST_PROFILE})
@DirtiesContext
@Profile(TestProfiles.DEFAULT_TEST_PROFILE)
public class KafkaIT
{
    @Test
    public void roundTrip()
    {
        // given
        final EventMessage inputEvent = EventMessage.newBuilder()
                                                    .setCustomerId("customer")
                                                    .setUuId("uuid-1")
                                                    .setDomainId("domain")
                                                    .setGroupId("group")
                                                    .setSourceId("source")
                                                    .setTime(Instant.now().toEpochMilli())
                                                    .setType("type")
                                                    .setCategory(EventCategoryEnum.EVENT_CATEGORY_CUSTOMER_DATA)
                                                    .putAllHeader(ImmutableMap.of())
                                                    .setPayload(ByteString.EMPTY)
                                                    .build();

        // when
        kafkaWriter.append(inputEvent);
        final EventMessage outputEvent = kafkaSupplier.get();

        // then
        assertThat(outputEvent).isEqualTo(inputEvent);
    }

    @PostConstruct
    void createKafkaSupplier()
    {
        this.kafkaSupplier = beanFactory.getBean(KafkaSupplier.class, new SubscriptionCriteria("FROM BEGINNING"));
    }

    private static final String TEST_TOPIC = "customer-domain";

    @ClassRule
    public static KafkaEmbedded kafkaEmbedded = new KafkaEmbedded(1, true, TEST_TOPIC);

    @Autowired
    private KafkaWriter kafkaWriter;

    @Autowired
    private BeanFactory beanFactory;

    private KafkaSupplier kafkaSupplier;
}
