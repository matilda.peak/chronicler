/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.storage.kafka;

import com.google.common.collect.ImmutableMap;
import com.google.protobuf.ByteString;
import matildapeak.chronicler.EventCategoryEnumOuterClass.EventCategoryEnum;
import matildapeak.chronicler.EventMessageOuterClass.EventMessage;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.TopicPartition;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.Instant;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

@SuppressWarnings("unchecked")
public class KafkaWriterTest
{
    @Before
    public void initMocks()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void append() throws InterruptedException, ExecutionException
    {
        // given
        final EventMessage eventMessage = makeEventMessage();
        this.kafkaWriter.topicNamePolicy = mock(TopicNamePolicy.class);
        this.kafkaWriter.partitionKeyPolicy = mock(PartitionKeyPolicy.class);

        given(kafkaWriter.topicNamePolicy.makeName(eventMessage)).willReturn("topic-name");
        given(kafkaWriter.partitionKeyPolicy.makeKey(eventMessage)).willReturn("partition-key");

        final RecordMetadata metadata = new RecordMetadata(
                new TopicPartition("topic-name", 0),
                0L, 0L, 0L, Long.valueOf(0L), 0, 0);
        final Future<RecordMetadata> future = mock(Future.class);
        given(future.get()).willReturn(metadata);

        given(kafkaProducer.send(any(ProducerRecord.class))).willReturn(future);

        // when
        kafkaWriter.append(eventMessage);

        // then
        then(kafkaWriter.topicNamePolicy).should().makeName(eventMessage);
        then(kafkaWriter.partitionKeyPolicy).should().makeKey(eventMessage);

        final ArgumentCaptor<ProducerRecord<String, byte[]>> producerRecord = ArgumentCaptor.forClass(ProducerRecord.class);
        then(kafkaProducer).should().send(producerRecord.capture());

        assertThat(producerRecord.getValue().topic()).isEqualTo("topic-name");
        assertThat(producerRecord.getValue().key()).isEqualTo("partition-key");
        assertThat(producerRecord.getValue().value()).isEqualTo(eventMessage.toByteArray());
    }

    @Test
    public void appendWhenSendFailsWithInterruptedException() throws ExecutionException, InterruptedException
    {
        // given
        final EventMessage eventMessage = makeEventMessage();
        this.kafkaWriter.topicNamePolicy = mock(TopicNamePolicy.class);
        this.kafkaWriter.partitionKeyPolicy = mock(PartitionKeyPolicy.class);

        given(kafkaWriter.topicNamePolicy.makeName(eventMessage)).willReturn("topic-name");
        given(kafkaWriter.partitionKeyPolicy.makeKey(eventMessage)).willReturn("partition-key");

        final Future<RecordMetadata> future = mock(Future.class);
        given(future.get()).willThrow(new InterruptedException("Computer says no!"));

        given(kafkaProducer.send(any(ProducerRecord.class))).willReturn(future);

        // when
        final Throwable thrown = catchThrowable(() -> kafkaWriter.append(eventMessage));

        // then
        assertThat(thrown).isInstanceOf(IllegalStateException.class)
                          .hasMessage("Caught exception while sending producer record for message with uuid ="
                                              + " [uuid-1]"
                                              + " to topic [topic-name]:"
                                              + " Computer says no!")
                          .hasCauseInstanceOf(InterruptedException.class);

        then(kafkaProducer).should().send(any(ProducerRecord.class));
        then(future).should().get();
    }

    @Test
    public void appendWhenSendFailsWithExecutionException() throws ExecutionException, InterruptedException
    {
        // given
        final EventMessage eventMessage = makeEventMessage();
        this.kafkaWriter.topicNamePolicy = mock(TopicNamePolicy.class);
        this.kafkaWriter.partitionKeyPolicy = mock(PartitionKeyPolicy.class);

        given(kafkaWriter.topicNamePolicy.makeName(eventMessage)).willReturn("topic-name");
        given(kafkaWriter.partitionKeyPolicy.makeKey(eventMessage)).willReturn("partition-key");

        final Future<RecordMetadata> future = mock(Future.class);
        given(future.get()).willThrow(new ExecutionException(new IllegalStateException("Computer says no!")));

        given(kafkaProducer.send(any(ProducerRecord.class))).willReturn(future);

        // when
        final Throwable thrown = catchThrowable(() -> kafkaWriter.append(eventMessage));

        // then
        assertThat(thrown).isInstanceOf(IllegalStateException.class)
                          .hasMessage("Caught exception while sending producer record for message with uuid ="
                                              + " [uuid-1]"
                                              + " to topic [topic-name]:"
                                              + " java.lang.IllegalStateException: Computer says no!")
                          .hasCauseInstanceOf(ExecutionException.class);

        then(kafkaProducer).should().send(any(ProducerRecord.class));
    }

    @Test
    public void defaultTopicNamePolicy()
    {
        // given
        final EventMessage eventMessage = makeEventMessage();
        // when
        final String topic = kafkaWriter.topicNamePolicy.makeName(eventMessage);

        // then
        assertThat(topic).isEqualTo("customer-domain");
    }

    @Test
    public void defaultPartitionKeyPolicy()
    {
        // given
        final EventMessage eventMessage = makeEventMessage();

        // when
        final String key = kafkaWriter.partitionKeyPolicy.makeKey(eventMessage);

        // then
        assertThat(key).isEqualTo("group-source");
    }

    private EventMessage makeEventMessage()
    {
        return EventMessage.newBuilder()
                           .setCustomerId("customer")
                           .setUuId("uuid-1")
                           .setDomainId("domain")
                           .setGroupId("group")
                           .setSourceId("source")
                           .setTime(Instant.now().toEpochMilli())
                           .setType("type")
                           .setCategory(EventCategoryEnum.EVENT_CATEGORY_CUSTOMER_DATA)
                           .putAllHeader(ImmutableMap.of())
                           .setPayload(ByteString.EMPTY)
                           .build();
    }

    @Mock
    private KafkaProducer<String, byte[]> kafkaProducer;

    @InjectMocks
    private KafkaWriter kafkaWriter;
}