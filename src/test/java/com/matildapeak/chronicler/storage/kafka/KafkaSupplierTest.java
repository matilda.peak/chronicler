/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.storage.kafka;

import com.google.protobuf.InvalidProtocolBufferException;
import com.matildapeak.chronicler.domain.SubscriptionCriteria;
import com.matildapeak.chronicler.storage.CancelledSupplierException;
import matildapeak.chronicler.EventMessageOuterClass.EventMessage;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.InterruptException;
import org.apache.kafka.common.errors.WakeupException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

@SuppressWarnings("unchecked")
public class KafkaSupplierTest
{
    @Before
    public void initMocks()
    {
        MockitoAnnotations.initMocks(this);

        // If the object is successfully created with the constructor, then Mockito won’t try the other strategies,
        // and KafkaSupplier has a constructor
        kafkaSupplier.kafkaConsumer =  kafkaConsumer;
    }

    @Test
    public void get() throws InvalidProtocolBufferException
    {
        // given
        final byte[] bytes = new byte[]{};
        final Iterator<ConsumerRecord<String, byte[]>> iterator = mock(Iterator.class);
        final ConsumerRecords<String, byte[]> records = mockRecords(bytes, 0, iterator);

        given(kafkaConsumer.poll(anyLong())).willReturn(records);
        willReturn(EventMessage.getDefaultInstance()).given(kafkaSupplier).from(bytes);

        // when
        final EventMessage result = kafkaSupplier.get();

        // then
        then(kafkaConsumer).should().poll(anyLong());
        then(records).should().iterator();
        then(iterator).should(times(1)).hasNext();
        then(iterator).should(times(1)).next();

        then(kafkaSupplier).should().from(bytes);
        then(kafkaConsumer).should().commitAsync(any());

        assertThat(result).isSameAs(EventMessage.getDefaultInstance());
    }

    @Test
    public void getWhenPayloadIsInvalid() throws InvalidProtocolBufferException
    {
        // given
        final byte[] invalidPayload = new byte[]{};
        final Iterator<ConsumerRecord<String, byte[]>> iteratorInvalidRecords = mock(Iterator.class);
        final ConsumerRecords<String, byte[]> invalidRecords
                = mockRecords(invalidPayload, 0, iteratorInvalidRecords);

        final byte[] validPayload = new byte[]{ 1 };
        final Iterator<ConsumerRecord<String, byte[]>> iteratorValidRecords = mock(Iterator.class);
        final ConsumerRecords<String, byte[]> validRecords
                = mockRecords(validPayload, 1, iteratorValidRecords);

        given(kafkaConsumer.poll(anyLong())).willReturn(invalidRecords, validRecords);

        willReturn(EventMessage.getDefaultInstance()).given(kafkaSupplier).from(validPayload);
        willThrow(new InvalidProtocolBufferException("Oops")).given(kafkaSupplier).from(invalidPayload);

        // when
        final EventMessage result = kafkaSupplier.get();

        // then
        then(kafkaConsumer).should(times(2)).poll(anyLong());
        then(invalidRecords).should().iterator();
        then(validRecords).should().iterator();

        then(iteratorInvalidRecords).should(times(1)).hasNext();
        then(iteratorInvalidRecords).should(times(1)).next();

        then(iteratorValidRecords).should(times(1)).hasNext();
        then(iteratorValidRecords).should(times(1)).next();

        then(kafkaSupplier).should().from(invalidPayload);
        then(kafkaSupplier).should().from(validPayload);

        then(kafkaConsumer).should(times(2)).commitAsync(any());

        assertThat(result).isSameAs(EventMessage.getDefaultInstance());
    }

    @Test
    public void kafkaConsumerGracefullyCloseIfWokenUpWhilePolling()
    {
        // given
        given(kafkaConsumer.poll(anyLong())).willThrow(new WakeupException());

        // when
        final Throwable thrown = catchThrowable(() -> kafkaSupplier.get());

        // then
        assertThat(thrown).isInstanceOf(CancelledSupplierException.class)
                          .hasMessage("KafkaConsumer woken up")
                          .hasCauseInstanceOf(WakeupException.class);

        then(kafkaConsumer).should().poll(anyLong());
        then(kafkaConsumer).should().close();
    }

    @Test
    public void kafkaConsumerDelegatesInterruptExceptionWhilePollingToCaller()
    {
        // given
        given(kafkaConsumer.poll(anyLong())).willThrow(new InterruptException("Computer says no!"));

        // when
        final Throwable thrown = catchThrowable(() -> kafkaSupplier.get());

        // then
        assertThat(thrown).isInstanceOf(InterruptException.class)
                          .hasMessage("Computer says no!");

        then(kafkaConsumer).should().poll(anyLong());
        then(kafkaConsumer).should(never()).close();
    }

    @Test
    public void cancel()
    {
        final Function<Boolean, Boolean> taskCanceller = mock(Function.class);
        given(taskCanceller.apply(false)).willReturn(true);

        final boolean cancelled = kafkaSupplier.cancel(taskCanceller);

        assertThat(cancelled).isTrue();
        then(kafkaConsumer).should().wakeup();
        then(taskCanceller).should().apply(false);
    }

    private ConsumerRecords<String, byte[]> mockRecords(
            final byte[] bytes,
            final int offset,
            final Iterator<ConsumerRecord<String, byte[]>> iterator)
    {
        final ConsumerRecord<String, byte[]> record = new ConsumerRecord<>("topic",
                                                                           1,
                                                                           offset,
                                                                           "event-" + offset,
                                                                           bytes);

        given(iterator.hasNext()).willReturn(true, false);
        given(iterator.next()).willReturn(record).willThrow(new NoSuchElementException("He went too far"));

        final ConsumerRecords<String, byte[]> records = mock(ConsumerRecords.class);
        given(records.iterator()).willReturn(iterator);

        return records;
    }

    @Mock
    private SubscriptionCriteria subscriptionCriteria;

    @Mock
    private KafkaConsumer<String, byte[]> kafkaConsumer;

    @InjectMocks
    @Spy
    private KafkaSupplier kafkaSupplier;
}