/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.storage.queue;

import com.matildapeak.chronicler.storage.CancelledSupplierException;
import matildapeak.chronicler.EventMessageOuterClass.EventMessage;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.concurrent.BlockingQueue;
import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

@SuppressWarnings("unchecked")
public class QueueSupplierTest
{
    @Before
    public void initMocks()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void get() throws InterruptedException
    {
        given(repository.take()).willReturn(EventMessage.getDefaultInstance());

        final EventMessage eventMessage = queueSupplier.get();

        assertThat(eventMessage).isSameAs(EventMessage.getDefaultInstance());
        then(repository).should().take();
    }

    @Test
    public void getHandlingInterruptWhenCancelled() throws InterruptedException
    {
        queueSupplier.cancelled.set(true);
        given(repository.take()).willThrow(new InterruptedException("Computer says no!"));

        final Throwable thrown = catchThrowable(() -> queueSupplier.get());

        then(repository).should().take();
        assertThat(thrown).isInstanceOf(CancelledSupplierException.class)
                          .hasMessage("Interrupted")
                          .hasCauseInstanceOf(InterruptedException.class);
    }

    @Test
    public void getHandlingInterruptWhenNotCancelled() throws InterruptedException
    {
        queueSupplier.cancelled.set(true);
        given(repository.take()).willThrow(new InterruptedException("Computer says no!"));

        final Throwable thrown = catchThrowable(() -> queueSupplier.get());

        then(repository).should().take();
        assertThat(thrown).isInstanceOf(RuntimeException.class)
                          .hasMessage("Interrupted")
                          .hasCauseInstanceOf(InterruptedException.class);
    }

    @Test
    public void cancel()
    {
        queueSupplier.cancelled.set(false);
        final Function<Boolean, Boolean> taskCanceller = mock(Function.class);
        given(taskCanceller.apply(true)).willReturn(true);

        final boolean cancelled = queueSupplier.cancel(taskCanceller);

        assertThat(cancelled).isTrue();
        assertThat(queueSupplier.cancelled.get()).isTrue();
        then(taskCanceller).should().apply(true);
    }

    @InjectMocks
    private QueueSupplier queueSupplier;

    @Mock
    private BlockingQueue<EventMessage> repository;
}