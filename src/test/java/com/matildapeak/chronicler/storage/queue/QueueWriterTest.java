/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.storage.queue;

import matildapeak.chronicler.EventMessageOuterClass.EventMessage;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.concurrent.BlockingQueue;

import static org.mockito.ArgumentMatchers.same;
import static org.mockito.BDDMockito.then;

public class QueueWriterTest
{
    @Before
    public void initMocks()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void append()
    {
        final EventMessage eventMessage = EventMessage.getDefaultInstance();

        queueWriter.append(eventMessage);

        then(repository).should().offer(same(eventMessage));
    }

    @InjectMocks
    private QueueWriter queueWriter;

    @Mock
    private BlockingQueue<EventMessage> repository;
}
