/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler;

import matildapeak.chronicler.EventMessageOuterClass.EventMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import javax.websocket.*;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

@Component
@ClientEndpoint(decoders = WebSocketClient.EventMessageDecoder.class)
public class WebSocketClient
{
    public static class EventMessageDecoder implements Decoder.BinaryStream<EventMessage>
    {

        @Override
        public EventMessage decode(final InputStream is) throws IOException
        {
            return EventMessage.parseFrom(is);
        }

        @Override
        public void init(EndpointConfig endpointConfig)
        {
        }

        @Override
        public void destroy()
        {
        }
    }

    public void connect(final URI uri) throws IOException, DeploymentException
    {
        this.connectionSession = ContainerProvider.getWebSocketContainer().connectToServer(this, uri);
    }

    @OnOpen
    public void onOpen(final Session session)
    {
        logger.info("Connected with session id {}", session.getId());
    }

    @OnMessage
    public void onMessage(final EventMessage message, final Session session)
    {
        logger.info("Received from session id {} message: {}", session.getId(), message);
        inbox.add(message);
    }

    @OnClose
    public void onClose(final Session session, final CloseReason closeReason)
    {
        logger.info("Session {} close because of {}", session.getId(), closeReason);
    }

    public BlockingQueue<EventMessage> getInbox()
    {
        return inbox;
    }

    @PreDestroy
    void close() throws IOException
    {
        if (connectionSession != null && connectionSession.isOpen())
        {
            connectionSession.close(
                    new CloseReason(CloseReason.CloseCodes.NORMAL_CLOSURE,
                                    "Shutting down"));
        }
    }

    private Session connectionSession;

    private final BlockingQueue<EventMessage> inbox = new LinkedBlockingQueue<>();

    private final Logger logger = LoggerFactory.getLogger(getClass());
}
