/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler;

import com.matildapeak.chronicler.storage.queue.QueueConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles({TestProfiles.DEFAULT_TEST_PROFILE, QueueConfiguration.QUEUE_PROFILE})
@DirtiesContext
public class ActuatorIT
{

    @Test
    public void healthAndInfoAndPrometheusDoesNotRequireCredentials() throws Exception
    {
        mvc.perform(get("/actuator/health"))
           .andExpect(status().isOk());

        mvc.perform(get("/actuator/info"))
           .andExpect(status().isOk());

        mvc.perform(get("/actuator/prometheus"))
           .andExpect(status().isOk());
    }

    @Test
    public void cannotListActuators() throws Exception
    {
        mvc.perform(get("/actuator"))
           .andExpect(status().isUnauthorized());
    }

    @Test
    public void actuatorsRequireCredentials() throws Exception
    {
        mvc.perform(get("/actuator/env"))
           .andExpect(status().isUnauthorized());

        mvc.perform(get("/actuator/threaddump"))
           .andExpect(status().isUnauthorized());

        mvc.perform(get("/actuator/beans"))
           .andExpect(status().isUnauthorized());

        mvc.perform(get("/actuator/loggers"))
           .andExpect(status().isUnauthorized());

        mvc.perform(get("/actuator/metrics"))
           .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(roles = SecurityConfiguration.ROLE_ACTUATOR)
    public void actuatorsCanBeAccessedWithCorrectCredentials() throws Exception
    {
        mvc.perform(get("/actuator/env"))
           .andExpect(status().isOk());

        mvc.perform(get("/actuator/threaddump"))
           .andExpect(status().isOk());

        mvc.perform(get("/actuator/beans"))
           .andExpect(status().isOk());

        mvc.perform(get("/actuator/loggers"))
           .andExpect(status().isOk());

        mvc.perform(get("/actuator/metrics"))
           .andExpect(status().isOk());
    }

    @Autowired
    private MockMvc mvc;
}
