/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler;

import com.matildapeak.chronicler.outbound.WebSocketChannelDescription;
import com.matildapeak.chronicler.storage.kafka.KafkaConfiguration;
import matildapeak.chronicler.EventMessageOuterClass.EventMessage;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.test.rule.KafkaEmbedded;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.websocket.DeploymentException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Full end-to-end test
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles({KafkaConfiguration.KAFKA_PROFILE, TestProfiles.DEFAULT_TEST_PROFILE})
@DirtiesContext
public class End2EndKafkaIT
{
    @Test
    public void roundTrip() throws IOException, DeploymentException, InterruptedException
    {

        // when
        // Post and event
        final String payload = "Hello World!";

        final ResponseEntity<String> pushResponse = restTemplate.postForEntity(
                "/inbound/customer/domain/group/source/type",
                payload.getBytes(StandardCharsets.UTF_8),
                String.class);

        assertThat(pushResponse.getStatusCode()).isEqualByComparingTo(HttpStatus.CREATED);

        // Connect to webSocket
        final ResponseEntity<WebSocketChannelDescription> channelDescription = restTemplate.postForEntity(
                "/outbound",
                "no criteria",
                WebSocketChannelDescription.class);

        assertThat(channelDescription.getStatusCode()).isEqualByComparingTo(HttpStatus.CREATED);
        webSocketClient.connect(channelDescription.getBody().getUri());

        try
        {
            // then
            final EventMessage eventMessage = webSocketClient.getInbox().poll(10, TimeUnit.SECONDS);

            assertThat(eventMessage.getCustomerId()).isEqualTo("customer");
            assertThat(eventMessage.getPayload().toStringUtf8()).isEqualTo(payload);
        }
        finally
        {
            webSocketClient.close();
        }
    }

    private static final String TEST_TOPIC = "customer-domain";

    @ClassRule
    public static KafkaEmbedded kafkaEmbedded = new KafkaEmbedded(1, true, TEST_TOPIC);

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private WebSocketClient webSocketClient;

    private final Logger logger = LoggerFactory.getLogger(getClass());
}
