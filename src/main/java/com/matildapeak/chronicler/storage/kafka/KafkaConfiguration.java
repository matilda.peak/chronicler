/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.storage.kafka;

import com.matildapeak.chronicler.util.SequenceIdGenerator;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;

import java.util.Properties;

@Configuration
@Profile(KafkaConfiguration.KAFKA_PROFILE)
public class KafkaConfiguration
{
    @Bean
    public SequenceIdGenerator consumerIdGenerator()
    {
        return new SequenceIdGenerator(chroniclerUUID + "consumer-");
    }

    @Bean
    public KafkaProducer<String, byte[]> kafkaProducer()
    {
        final Properties properties = new Properties();

        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.put(ProducerConfig.CLIENT_ID_CONFIG, chroniclerUUID + "producer");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class.getName());
        properties.put(ProducerConfig.COMPRESSION_TYPE_CONFIG, "snappy");
        properties.put(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, 1);

        return new KafkaProducer<>(properties);
    }

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public KafkaConsumer<String, byte[]> kafkaConsumer()
    {
        final Properties properties = new Properties();

        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.put(ConsumerConfig.CLIENT_ID_CONFIG, chroniclerUUID + "consumer");
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, consumerIdGenerator().generate());
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ByteArrayDeserializer.class.getName());
        properties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
//        properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest"); // We use seekTo in the consumer

        // One record at a time
        properties.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 1);

        return new KafkaConsumer<>(properties);
    }

    public static final String KAFKA_PROFILE = "kafka";

    @Value("${com.matildapeak.chronicler.storage.kafka.bootstrap.servers}")
    private String bootstrapServers;

    @Autowired
    private String chroniclerUUID;
}
