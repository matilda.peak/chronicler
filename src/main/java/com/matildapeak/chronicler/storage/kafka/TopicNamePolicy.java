/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.storage.kafka;

import matildapeak.chronicler.EventMessageOuterClass.EventMessage;

/**
 * Signature for all classes that work out the {@code Kafka topic} name from the given {@link EventMessage}
 */
public interface TopicNamePolicy
{
    String makeName(EventMessage eventMessage);
}
