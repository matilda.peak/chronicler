/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.storage.kafka;

import com.matildapeak.chronicler.storage.EventLogWriter;
import io.micrometer.core.annotation.Timed;
import matildapeak.chronicler.EventMessageOuterClass.EventMessage;
import net.jcip.annotations.ThreadSafe;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutionException;

@Component
@ThreadSafe
@Profile(KafkaConfiguration.KAFKA_PROFILE)
public class KafkaWriter implements EventLogWriter
{
    @Timed("kafka.write")
    @Override
    public void append(final EventMessage eventMessage)
    {
        logger.debug("Entering append with uuid = {}", eventMessage.getUuId());

        final String topic = topicNamePolicy.makeName(eventMessage);
        logger.trace("Topic = {}", topic);

        final String key = partitionKeyPolicy.makeKey(eventMessage);
        logger.trace("Key = {}", key);

        final ProducerRecord<String, byte[]> producerRecord
                = new ProducerRecord<>(topic, key, eventMessage.toByteArray());

        try
        {
            final RecordMetadata metadata = kafkaProducer.send(producerRecord).get();

            logger.trace("Record send to topic = [{}], partition = [{}], offset = [{}]",
                         metadata.topic(),
                         metadata.partition(),
                         metadata.offset());
        }
        catch (final InterruptedException | ExecutionException ex)
        {
            throw new IllegalStateException(
                    "Caught exception while sending producer record for message with uuid = ["
                            + eventMessage.getUuId()
                            + "] to topic ["
                            + topic
                            + "]: "
                            + ex.getMessage(), ex);
        }

        logger.debug("Leaving append with uuid = {}", eventMessage.getUuId());
    }

    @Autowired(required = false)
    public void setTopicNamePolicy(final TopicNamePolicy topicNamePolicy)
    {
        this.topicNamePolicy = topicNamePolicy;
    }

    @Autowired(required = false)
    public void setPartitionKeyPolicy(final PartitionKeyPolicy partitionKeyPolicy)
    {
        this.partitionKeyPolicy = partitionKeyPolicy;
    }

    TopicNamePolicy topicNamePolicy =
            eventMessage -> eventMessage.getCustomerId() + '-' + eventMessage.getDomainId();

    PartitionKeyPolicy partitionKeyPolicy =
            eventMessage -> eventMessage.getGroupId() + '-' + eventMessage.getSourceId();

    @Autowired
    private KafkaProducer<String, byte[]> kafkaProducer;

    private final Logger logger = LoggerFactory.getLogger(getClass());
}
