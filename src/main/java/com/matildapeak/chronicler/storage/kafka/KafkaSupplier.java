/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.storage.kafka;

import com.google.common.collect.ImmutableList;
import com.google.protobuf.InvalidProtocolBufferException;
import com.matildapeak.chronicler.domain.SubscriptionCriteria;
import com.matildapeak.chronicler.outbound.EventMessageSupplier;
import com.matildapeak.chronicler.storage.CancelledSupplierException;
import io.micrometer.core.annotation.Timed;
import matildapeak.chronicler.EventMessageOuterClass.EventMessage;
import net.jcip.annotations.NotThreadSafe;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.WakeupException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Iterator;
import java.util.Map;
import java.util.function.Function;

@Component
@NotThreadSafe
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class KafkaSupplier implements EventMessageSupplier
{
    @Autowired
    KafkaSupplier(final SubscriptionCriteria subscriptionCriteria)
    {
        logger.debug("Built");
        this.subscriptionCriteria = subscriptionCriteria;
    }

    @Timed("kafka.read")
    @Override
    public EventMessage get()
    {
        logger.debug("Entering get");

        // TODO: should default end-of-supplier
        EventMessage result = null;

        try
        {
            while (result == null)
            {
                logger.debug("Polling");
                final ConsumerRecords<String, byte[]> records = kafkaConsumer.poll(Long.MAX_VALUE);

                logger.debug("Iterating over records");
                final Iterator<ConsumerRecord<String, byte[]>> iterator = records.iterator();
                if (iterator.hasNext())
                {
                    final ConsumerRecord<String, byte[]> record = iterator.next();
                    try
                    {
                        logger.debug("translating payload of {} bytes into event message", record.value().length);
                        result = from(record.value());
                    }
                    catch (final InvalidProtocolBufferException ex)
                    {
                        logger.error("Message payload is not a valid EventMessage: {}", ex.getMessage(), ex);
                        // TODO: log base64 value of byte
                    }

                    logger.debug("Committing");
                    kafkaConsumer.commitAsync(
                            (final Map<TopicPartition, OffsetAndMetadata> offsets, final Exception ex) ->
                            {
                                if (ex != null)
                                {
                                    logger.error("Commit failed for offsets {} because of {}", offsets, ex.getMessage(), ex);
                                }
                            });
                }
            }
        }
        catch (final WakeupException ex)
        {
            logger.debug("Closing KafkaConsumer");
            try
            {
                kafkaConsumer.close();
            }
            catch (final RuntimeException closeException)
            {
                logger.error("Caught exception when closing the KafkaConsumer: {}", ex.getMessage(), ex);
            }

            logger.debug("Done closing KafkaConsumer");
            throw new CancelledSupplierException("KafkaConsumer woken up", ex);
        }

        logger.debug("Leaving get: {}", result.getUuId());
        return result;
    }

    @Override
    public boolean cancel(final Function<Boolean, Boolean> taskCanceller)
    {
        logger.info("Waking up KafkaConsumer: {}", kafkaConsumer);
        kafkaConsumer.wakeup();

        return taskCanceller.apply(false); // Do not interrupt
    }

    EventMessage from(final byte[] bytes) throws InvalidProtocolBufferException
    {
        return EventMessage.parseFrom(bytes);
    }

    @PostConstruct
    void subscribe()
    {
        logger.info("Subscribing to topic: {}", "customer-domain");

        // TODO: from subscription criteria
        kafkaConsumer.subscribe(ImmutableList.of("customer-domain"));

        // TODO: only when seeking to a position that is not the end
        if (!subscriptionCriteria.fromEnd())
        {
            kafkaConsumer.poll(0);
            kafkaConsumer.commitSync();
            kafkaConsumer.seekToBeginning(ImmutableList.of());
        }
    }

    private final SubscriptionCriteria subscriptionCriteria;

    @Autowired
    KafkaConsumer<String, byte[]> kafkaConsumer;

    private final Logger logger = LoggerFactory.getLogger(getClass());
}
