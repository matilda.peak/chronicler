/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.storage.queue;

import com.matildapeak.chronicler.outbound.EventMessageSupplier;
import com.matildapeak.chronicler.storage.CancelledSupplierException;
import matildapeak.chronicler.EventMessageOuterClass.EventMessage;
import net.jcip.annotations.ThreadSafe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;

@Component
@ThreadSafe
@Profile(QueueConfiguration.QUEUE_PROFILE)
public class QueueSupplier implements EventMessageSupplier
{
    @Override
    public EventMessage get()
    {
        try
        {
            return repository.take();
        }
        catch (final InterruptedException ex)
        {
            if (cancelled.getAndSet(false))
            {
                throw new CancelledSupplierException("Interrupted", ex);
            }
            else
            {
                throw new RuntimeException("Interrupted", ex);
            }
        }
    }

    @Override
    public boolean cancel(final Function<Boolean, Boolean> taskCanceller)
    {
        cancelled.set(true);

        return taskCanceller.apply(true);
    }

    final AtomicBoolean cancelled = new AtomicBoolean(false);

    @Autowired
    private BlockingQueue<EventMessage> repository;
}
