/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.storage.queue;

import matildapeak.chronicler.EventMessageOuterClass.EventMessage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

@Configuration
@Profile(QueueConfiguration.QUEUE_PROFILE)
public class QueueConfiguration
{
    public static final String QUEUE_PROFILE = "queue";

    @Bean
    public BlockingQueue<EventMessage> repository()
    {
        return new LinkedBlockingQueue<>();
    }
}
