/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.storage.queue;

import com.matildapeak.chronicler.storage.EventLogWriter;
import matildapeak.chronicler.EventMessageOuterClass.EventMessage;
import net.jcip.annotations.ThreadSafe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.concurrent.BlockingQueue;

@Component
@ThreadSafe
@Profile(QueueConfiguration.QUEUE_PROFILE)
public class QueueWriter implements EventLogWriter
{
    @Override
    public void append(final EventMessage eventMessage)
    {
        logger.debug("Entering append with uuid = {}", eventMessage.getUuId());

        repository.offer(eventMessage);

        logger.debug("Leaving append");
    }

    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
    @Autowired
    private BlockingQueue<EventMessage> repository;

    private final Logger logger = LoggerFactory.getLogger(getClass());
}
