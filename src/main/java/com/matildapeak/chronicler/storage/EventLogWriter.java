/*
 * Copyright (c) 2018 Matilda Peak - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.storage;

import matildapeak.chronicler.EventMessageOuterClass.EventMessage;

/**
 * Signature for all writes of event
 *
 * @author Alan Christie
 */
public interface EventLogWriter
{
    void append(EventMessage eventMessage);
}
