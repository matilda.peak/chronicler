/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.storage;

/**
 * Flags an interruption while reading data from the supplier
 */
public class CancelledSupplierException extends RuntimeException
{
    public CancelledSupplierException(final String message)
    {
        super(message);
    }

    public CancelledSupplierException(final String message, final Throwable cause)
    {
        super(message, cause);
    }
}
