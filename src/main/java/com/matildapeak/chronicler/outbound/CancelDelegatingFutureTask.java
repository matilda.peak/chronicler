/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.outbound;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

class CancelDelegatingFutureTask<T> extends FutureTask<T>
{

    CancelDelegatingFutureTask(final Callable<T> callable)
    {
        super(callable);
        this.cancellable = (Cancellable) callable;
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning)
    {
        return this.cancellable.cancel(super::cancel);
    }

    private final Cancellable cancellable;
}
