/*
 * Copyright (c) 2018 Matilda Peak - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package com.matildapeak.chronicler.outbound;

import com.matildapeak.chronicler.domain.SubscriptionCriteria;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tags;
import matildapeak.chronicler.EventMessageOuterClass.EventMessage;
import net.jcip.annotations.ThreadSafe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.PostConstruct;
import java.net.MalformedURLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.matildapeak.chronicler.outbound.OutboundController.CHANNEL_ID_PATH_PARAMETER;

/**
 * The service that manages the outbound channels by which {@link EventMessage} are streamed out of chronicler.
 *
 * @author Alan Christie
 */
@Service
@ThreadSafe
class OutboundService
{
    /**
     * Allocates the resources for the new communication channel
     * <p>
     *
     * @param criteria
     * @param uriComponentsBuilder
     * @return
     * @throws MalformedURLException
     */
    public WebSocketChannelDescription prepareChannel(
            final SubscriptionCriteria criteria,
            final UriComponentsBuilder uriComponentsBuilder) throws MalformedURLException
    {
        logger.debug("Entering prepareChannel");

        final WebSocketChannelDescription description = makeWebSocketChannelDescription(uriComponentsBuilder);

        channels.put(description.getId(), beanFactory.getBean(OutboundChannel.class, criteria));

        logger.debug("Leaving prepareChannel");

        return description;
    }

    /**
     * Opens the channel by kicking off the process that reads events of the log and sends them down the webSocket
     * associated to the channel
     *
     * @param webSocketSession
     * @throws IllegalStateException
     * @throws IllegalArgumentException
     */
    public void openChannel(final WebSocketSession webSocketSession)
            throws IllegalStateException, IllegalArgumentException
    {
        logger.debug("Entering openChannel");

        logger.info("webSocketSession = {}", webSocketSession);

        final String channelId = (String) webSocketSession.getAttributes().get(CHANNEL_ID_PATH_PARAMETER);
        if (channelId != null)
        {
            final OutboundChannel channel = channels.get(Long.valueOf(channelId));
            if (channel != null)
            {
                logger.debug("Opening channel {}", channelId);
                channel.open(webSocketSession, channelId);
            }
            else
            {
                throw new IllegalArgumentException("No channel found for id = " + channelId);
            }
        }
        else
        {
            throw new IllegalStateException("WebSocketSession is missing the channel ID");
        }

        logger.debug("Leaving openChannel");
    }

    /**
     * Closes the channel for which the connection of the given {@link WebSocketSession} has been closed.
     *
     * @param webSocketSession
     * @throws IllegalStateException
     * @throws IllegalArgumentException
     */
    public void closeChannel(final WebSocketSession webSocketSession)
            throws IllegalStateException, IllegalArgumentException
    {
        logger.debug("Entering closeChannel");

        logger.info("webSocketSession = {}", webSocketSession);

        final String channelId = (String) webSocketSession.getAttributes().get(CHANNEL_ID_PATH_PARAMETER);
        if (channelId != null)
        {
            final OutboundChannel channel = channels.remove(Long.valueOf(channelId));
            if (channel != null)
            {
                logger.debug("Closing channel {}", channelId);
                channel.close();
            }
            else
            {
                throw new IllegalArgumentException("No channel found for id = " + channelId);
            }
        }
        else
        {
            throw new IllegalStateException("WebSocketSession is missing the channel ID");
        }

        logger.debug("Leaving closeChannel");
    }

    /**
     * Closes the channel identified by the given {@code channelId}. This is used from the REST interface
     * via a {@code DELETE} request
     *
     * @param channelId the identifier of the channel to close
     * @throws IllegalArgumentException is thrown if there is no channel associated to the given identifier
     */
    public void closeChannel(final long channelId)
            throws IllegalArgumentException
    {
        logger.debug("Entering closeChannel");

        final OutboundChannel channel = channels.remove(channelId);
        if (channel != null)
        {
            logger.debug("Closing channel {}", channelId);
            channel.close();
        }
        else
        {
            throw new IllegalArgumentException("No channel found for id = " + channelId);
        }

        logger.debug("Leaving closeChannel");
    }

    WebSocketChannelDescription makeWebSocketChannelDescription(
            UriComponentsBuilder uriComponentsBuilder)
    {
        return new WebSocketChannelDescription(uriComponentsBuilder);
    }

    @PostConstruct
    void setupMetrics()
    {
        logger.info("Added gauge on number of channels");
        Metrics.gaugeMapSize("outbound.channels", Tags.empty(), channels);
    }

    /**
     * Maps ids to channels
     */
    @SuppressWarnings("CanBeFinal")
    private Map<Long, OutboundChannel> channels = new ConcurrentHashMap<>();

    @Autowired
    private BeanFactory beanFactory;

    private final Logger logger = LoggerFactory.getLogger(getClass());
}
