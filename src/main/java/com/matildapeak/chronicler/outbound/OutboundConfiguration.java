/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.outbound;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Configuration
public class OutboundConfiguration
{
    @Bean
    public ExecutorService outboundExecutorService()
    {
        return new ThreadPoolExecutor(0, Integer.MAX_VALUE,
                                      60L, TimeUnit.SECONDS,
                                      new SynchronousQueue<>(),
                                      outboundThreadFactory())
        {
            @Override
            protected <T> RunnableFuture<T> newTaskFor(final Callable<T> callable)
            {
                return new CancelDelegatingFutureTask<>(callable);
            }
        };
    }

    @Bean
    public ThreadFactory outboundThreadFactory()
    {
        return new ThreadFactoryBuilder().setNameFormat("outbound-%s").build();
    }
}
