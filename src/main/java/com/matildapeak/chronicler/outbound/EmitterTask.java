/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.outbound;

import com.matildapeak.chronicler.storage.CancelledSupplierException;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Timer;
import matildapeak.chronicler.EventMessageOuterClass.EventMessage;
import net.jcip.annotations.NotThreadSafe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.WebSocketSession;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.concurrent.Callable;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * A task that reads {@link EventMessage} instances from the given {@code Supplier}
 * and sends them down the given {@code WebSocket}
 * <p>
 */
@Component
@NotThreadSafe
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class EmitterTask implements Callable<Void>, Cancellable
{
    public EmitterTask(
            final EventMessageSupplier supplier,
            final WebSocketSession session,
            final String channelId)
    {
        this.supplier = supplier;
        this.session = session;
        this.channelId = channelId;
    }

    @PostConstruct
    protected void setupTimer()
    {
        this.timer = Metrics.timer("outbound.send", "channel", channelId);
    }

    @Override
    public Void call() throws Exception
    {
        logger.debug("Entering call for channel [{}]", channelId);

        try
        {
            Stream.generate(supplier)
                  .forEach(event ->
                           {
                               logger.trace("Sending vi channel [{}] event: {}", channelId, event);

                               timer.record(() ->
                                            {
                                                try
                                                {
                                                    session.sendMessage(new BinaryMessage(event.toByteArray()));
                                                }
                                                catch (final IOException ex)
                                                {
                                                    throw new UncheckedIOException(ex);
                                                }
                                            });

                               logger.trace("Event sent");
                           });
        }
        catch (final CancelledSupplierException ex)
        {
            logger.info("Supplier cancelled, no more messages will be sent trough channel [" + channelId + "]");
        }
        catch (final RuntimeException ex)
        {
            if (ex.getCause() != null && ex.getCause() instanceof InterruptedException)
            {
                throw (InterruptedException) ex.getCause();
            }
            else
            {
                throw ex;
            }
        }
        finally
        {
            try
            {
                logger.info("Closing webSocket session for channel [{}]", channelId);
                session.close();
            }
            catch (final IOException ex)
            {
                logger.error("Caught error when closing webSocket session: {}", ex.getMessage(), ex);
            }
        }

        logger.debug("Leaving call for channel [{}]", channelId);
        return null;
    }

    @Override
    public boolean cancel(final Function<Boolean, Boolean> taskCanceller)
    {
        return supplier.cancel(taskCanceller);
    }

    /**
     * The supplier to read events from
     */
    private final EventMessageSupplier supplier;

    /**
     * The webSocket to send the messages through
     */
    private final WebSocketSession session;

    /**
     * The channel identifier this task is emitting to
     */
    private final String channelId;

    /**
     * Timer for the send operation
     */
    protected Timer timer;

    private final Logger logger = LoggerFactory.getLogger(getClass());
}
