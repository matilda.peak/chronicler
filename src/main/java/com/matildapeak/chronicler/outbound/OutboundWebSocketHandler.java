/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.outbound;

import com.google.common.eventbus.EventBus;
import net.jcip.annotations.ThreadSafe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;

/**
 * The handler of webSocket sessions. Instantiated by {@link WebSocketConfig}
 */
@ThreadSafe
class OutboundWebSocketHandler extends AbstractWebSocketHandler
{
    @Override
    public void afterConnectionEstablished(final WebSocketSession session)
    {
        logger.info("Connection established for [{}]", session);
        outboundService.openChannel(session);
    }

    @Override
    public void handleTransportError(final WebSocketSession session, final Throwable exception) throws Exception
    {
        logger.error("Handling transport error on session [{}]: {}", session,  exception.getMessage());
        session.close(CloseStatus.SERVER_ERROR);
    }

    @Override
    public void afterConnectionClosed(final WebSocketSession session, final CloseStatus status)
    {
        logger.info("Closing connection for session [{}] with status {}", session, status);
        try
        {
            outboundService.closeChannel(session);
        }
        catch (final RuntimeException ex)
        {
            logger.error("Caught error while closing channel for session {} : {}", session, ex.getMessage(), ex);
        }
    }

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private OutboundService outboundService;

    @Autowired
    private EventBus internalEventBus;
}
