/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.outbound;

import java.util.function.Function;

/**
 * Signature for all cancellable classes run in a task
 */
public interface Cancellable
{
    /**
     * Asks to cancel the current operation and provides a reference to the function in charge of cancelling the task,
     * should the cancelling logic require to cancel it as well.
     *
     * @param taskCanceller a function that can be used to cancel the current task (thread) currently running the operation
     * @return whether the operation (and optionally the task) has been cancelled
     */
    boolean cancel(Function<Boolean, Boolean> taskCanceller);
}
