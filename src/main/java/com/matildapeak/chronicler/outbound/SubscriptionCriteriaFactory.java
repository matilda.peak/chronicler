/*
 * Copyright (c) 2018 Matilda Peak - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */
package com.matildapeak.chronicler.outbound;

import com.matildapeak.chronicler.domain.SubscriptionCriteria;
import net.jcip.annotations.Immutable;
import org.springframework.stereotype.Component;

/**
 * @author Alan Christie
 */
@Component
@Immutable
class SubscriptionCriteriaFactory
{

    public SubscriptionCriteria make(final String criteria)
    {
        return new SubscriptionCriteria(criteria);
    }
}
