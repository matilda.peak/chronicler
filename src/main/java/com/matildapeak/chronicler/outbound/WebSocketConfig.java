/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.outbound;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.server.standard.ServletServerContainerFactoryBean;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;

import static com.matildapeak.chronicler.outbound.OutboundController.CHANNEL_ID_PATH_PARAMETER;

/**
 * Using Spring specific websocket API
 * <p>
 * Described in http://docs.spring.io/spring/docs/current/spring-framework-reference/html/websocket.html
 */
@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer
{
    @Override
    public void registerWebSocketHandlers(final WebSocketHandlerRegistry registry)
    {
        registry.addHandler(outboundWebSocketHandler(), CHANNEL_PATH)
                .addInterceptors(new HttpSessionHandshakeInterceptor(),
                                 new UriTemplateHandshakeInterceptor());
    }

    @Bean
    public WebSocketHandler outboundWebSocketHandler()
    {
        return new OutboundWebSocketHandler();
    }

    @Bean
    public ServletServerContainerFactoryBean createWebSocketContainer()
    {
        final ServletServerContainerFactoryBean container = new ServletServerContainerFactoryBean();
        container.setMaxSessionIdleTimeout(600000L);
        container.setAsyncSendTimeout(writeTimeout);
        container.setMaxTextMessageBufferSize(8192);
        container.setMaxBinaryMessageBufferSize(8192);
        return container;
    }

    static final String CHANNEL_PATH = "/channel/{" + CHANNEL_ID_PATH_PARAMETER + "}";

    @Value("${com.matildapeak.chronicler.outbound.writetimeout:10000}")
    private long writeTimeout;
}