/*
 * Copyright (c) 2018 Matilda Peak - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited. Proprietary and confidential.
 */
package com.matildapeak.chronicler.outbound;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.jcip.annotations.ThreadSafe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.MalformedURLException;

/**
 * Endpoint for the outbound interface
 *
 * @author Alan Christie
 */
@RestController
@RequestMapping(OutboundController.OUTBOUND_PATH)
@ThreadSafe
class OutboundController
{
    @PostMapping(
            consumes = {MediaType.TEXT_PLAIN_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE},
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(
            value = "Creates a new channel with the given criteria",
            notes = "The websocket associated to the channel is not opened until connected. " +
                    "It returns a structure with the unique identifier of the channel, " +
                    "the websocket uri to connect to start receiving events " +
                    "and the version number of the websocket protocol",
            nickname = "openChannel")
    public ResponseEntity<WebSocketChannelDescription> open(
            @ApiParam("Event filtering and selecting criteria")
            @RequestBody String criteria,
            final UriComponentsBuilder uriBuilder) throws MalformedURLException
    {
        logger.debug("Entering open");
        // TODO: any authentication should've happened before

        final WebSocketChannelDescription channel =
                eventService.prepareChannel(
                        subscriptionCriteriaFactory.make(criteria),
                        uriBuilder.cloneBuilder());

        // Set the location of the newly created resource
        final UriComponents uriComponents = uriBuilder.path(CHANNEL_PATH).buildAndExpand(channel.getId());
        final HttpHeaders headers = new HttpHeaders();
        headers.setLocation(uriComponents.toUri());

        logger.debug("Leaving open");
        return new ResponseEntity<>(channel, headers, HttpStatus.CREATED);
    }

    @DeleteMapping("/{" + CHANNEL_ID_PATH_PARAMETER + "}")
    @ApiOperation(
            value = "Closes the channel identified by the given path parameter",
            notes = "By closing the channel we effectively close the associated websocket. " +
                    "Alternative method to closing the websocket by just disconnecting from it.",
            nickname = "closeChannel")
    public ResponseEntity<Long> close(
            @ApiParam("Unique identifier of the channel to close")
            @PathVariable final long channelId)
    {
        logger.debug("Entering close");

        try
        {
            eventService.closeChannel(channelId);
            logger.info("Channel [{}] has been closed", channelId);
        }
        catch (final IllegalArgumentException ex)
        {
            logger.error("Unable to close channel: {}", ex.getMessage(), ex);
            return new ResponseEntity<>(channelId, HttpStatus.NOT_FOUND);
        }

        logger.debug("Leaving close");
        return new ResponseEntity<>(channelId, HttpStatus.OK);
    }


    static final String OUTBOUND_PATH = "/outbound";

    static final String CHANNEL_ID_PATH_PARAMETER = "channelId";

    private static final String CHANNEL_PATH = OUTBOUND_PATH + "/{" + CHANNEL_ID_PATH_PARAMETER + "}";

    @Autowired
    private SubscriptionCriteriaFactory subscriptionCriteriaFactory;

    @Autowired
    private OutboundService eventService;

    /**
     * Our logger
     */
    private final Logger logger = LoggerFactory.getLogger(getClass());
}
