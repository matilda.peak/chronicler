/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.outbound;

import com.matildapeak.chronicler.domain.SubscriptionCriteria;
import net.jcip.annotations.NotThreadSafe;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import javax.annotation.PreDestroy;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 * The abstraction of a communication channel between chronicler and the client consuming the events.
 * <p>
 * It holds a reference to the task reading events from the log and sending them down the webSocket channel.
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@NotThreadSafe
class OutboundChannel
{
    public OutboundChannel(final SubscriptionCriteria subscriptionCriteria)
    {
        this.subscriptionCriteria = subscriptionCriteria;
    }

    void open(final WebSocketSession webSocketSession, final String channelId)
    {
        final EventMessageSupplier supplier = beanFactory.getBean(EventMessageSupplier.class, subscriptionCriteria);

        this.emitterTask = outboundExecutorService.submit(
                beanFactory.getBean(
                        EmitterTask.class,
                        supplier,
                        webSocketSession,
                        channelId));
    }

    @PreDestroy
    void close()
    {
        if (emitterTask != null)
        {
            final Future<Void> task = emitterTask;
            emitterTask = null;

            task.cancel(false); // value is actually ignored
        }
    }

    /**
     * Selects which events to pass on down the channel
     */
    private final SubscriptionCriteria subscriptionCriteria;

    Future<Void> emitterTask = null;

    @Autowired
    BeanFactory beanFactory;

    @Autowired
    ExecutorService outboundExecutorService;
}
