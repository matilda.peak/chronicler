/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.outbound;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.Serializable;
import java.net.URI;
import java.util.concurrent.atomic.AtomicLong;

import static com.matildapeak.chronicler.outbound.WebSocketConfig.CHANNEL_PATH;

/**
 * Describes the websocket the client has to connect to to receive events.
 * <p>
 * TODO: add resourceURL property
 */
public class WebSocketChannelDescription implements Serializable
{
    WebSocketChannelDescription(final UriComponentsBuilder uriComponentsBuilder)
    {
        this.id = ID_GENERATOR.incrementAndGet();

        final UriComponents uriComponents = uriComponentsBuilder
                .scheme("ws")
                .replacePath(CHANNEL_PATH)
                .buildAndExpand(Long.toString(this.id));

        this.uri = uriComponents.encode().toUri();
        logger.info("WebSocket URI: {}", uri);

        this.version = VERSION;
    }

    public long getId()
    {
        return id;
    }

    public URI getUri()
    {
        return uri;
    }

    @JsonCreator
    WebSocketChannelDescription(final long id, final URI uri, final String version)
    {
        this.id = id;
        this.uri = uri;
        this.version = version;
    }

    static final AtomicLong ID_GENERATOR = new AtomicLong();

    private static final String VERSION = System.getenv("SOLUTION_VERSION");

    private static final long serialVersionUID = 1L;

    /**
     * The ID of this webSocket channel
     */
    private final long id;

    /**
     * The URL of the webSocket
     */
    private final URI uri;

    /**
     * The version of the protocol to use in this webSocket
     */
    public final String version;

    /**
     * Our logger
     */
    @SuppressWarnings("FieldCanBeLocal")
    @JsonIgnore
    private final Logger logger = LoggerFactory.getLogger(getClass());
}
