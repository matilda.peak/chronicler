/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.outbound;

import matildapeak.chronicler.EventMessageOuterClass.EventMessage;

import java.util.function.Supplier;

public interface EventMessageSupplier extends Supplier<EventMessage>, Cancellable
{
}
