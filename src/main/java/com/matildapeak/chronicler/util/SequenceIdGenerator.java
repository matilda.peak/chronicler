/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.util;

import net.jcip.annotations.ThreadSafe;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Generates a sequence of identifiers by appending a unique sequence value to the given prefix.
 */
@ThreadSafe
public class SequenceIdGenerator
{
    public SequenceIdGenerator(final String prefix)
    {
        this.prefix = prefix;
    }

    /**
     * @return a unique uuid based on the configured prefix and the current value of the internal atomic counter.
     */
    public String generate()
    {
        return prefix + Long.toHexString(eventCounter.incrementAndGet());
    }

    private final AtomicLong eventCounter = new AtomicLong();

    private final String prefix;
}
