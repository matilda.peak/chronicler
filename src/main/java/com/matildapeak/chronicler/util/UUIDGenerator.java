/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.util;


import com.google.common.io.BaseEncoding;
import net.jcip.annotations.Immutable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.UUID;

/**
 * Generates a unique identifier as the bas64 representation of a compressed edition of a UUID version 4
 */
@Immutable
public class UUIDGenerator
{
    /**
     * Factory for this generator identifier
     *
     * @return a base64 representation of a UUID version 4
     */
    public String makeId()
    {
        final UUID uuid = UUID.randomUUID();
        final ByteBuffer buffer = ByteBuffer.wrap(new byte[16]);
        buffer.putLong(uuid.getMostSignificantBits());
        buffer.putLong(uuid.getLeastSignificantBits());
        buffer.rewind();

        final String id = BaseEncoding.base64Url().omitPadding().encode(buffer.array());

        logger.info("ID (hex) = {}", BaseEncoding.base16().lowerCase().encode(buffer.array()));
        logger.info("ID       = {}", id);

        return id;
    }

    private final Logger logger = LoggerFactory.getLogger(UUIDGenerator.class);
}
