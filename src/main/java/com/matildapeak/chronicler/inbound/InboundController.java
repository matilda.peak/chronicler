/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.inbound;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import matildapeak.chronicler.EventMessageOuterClass.EventMessage;
import net.jcip.annotations.ThreadSafe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Callable;

@RestController
@RequestMapping("/inbound")
@ThreadSafe
class InboundController
{
    @PostMapping(
            path = "/{customer}/{domain}/{group}/{source}/{type}",
            consumes = MediaType.APPLICATION_OCTET_STREAM_VALUE,
            produces = MediaType.TEXT_PLAIN_VALUE)
    @ApiOperation(
            value = "Records an event in chronicler",
            notes = "Events are grouped by an identifier resulting of joining 'customer' and 'domain', " +
                    "and order is guaranteed between events with the same 'group-source' concatenated value",
            nickname = "postEvent")
    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public Callable<ResponseEntity<String>> push(
            @ApiParam("Unique identifier of the customer producing the event")
            @PathVariable final String customer,
            @ApiParam("Unique identifier for the domain within the customer the event is coming from")
            @PathVariable final String domain,
            @ApiParam("Unique identifier for the group within the domain the event is coming from")
            @PathVariable final String group,
            @ApiParam("Unique identifier for the source within the group the event is coming from")
            @PathVariable final String source,
            @ApiParam("Type of the event as defined by the customer")
            @PathVariable final String type,
            @ApiParam("Meaningful properties of the event that can be used without having to decode the body")
            @RequestParam final Map<String, String> header,
            @ApiParam(
                    value = "Body of the event, if any",
                    allowEmptyValue = true)
            @RequestBody final Optional<byte[]> payload)
    {
        logger.debug("Entering push");

        final EventMessage eventMessage = eventMessageFactory.make(
                customer,
                domain,
                group,
                source,
                type,
                header,
                payload);

        logger.trace("Event = {}", eventMessage);

        final Callable<ResponseEntity<String>> insertionTask = () ->
        {
            logger.debug("Pushing eventMessage with uuid = {}", eventMessage.getUuId());

            inboundService.push(eventMessage);
            return new ResponseEntity<>(eventMessage.getUuId(), HttpStatus.CREATED);
        };

        logger.debug("Leaving push, uuid = {}", eventMessage.getUuId());
        return insertionTask;
    }

    @Autowired
    private InboundService inboundService;

    @Autowired
    private EventMessageFactory eventMessageFactory;

    /**
     * Our logger
     */
    private final Logger logger = LoggerFactory.getLogger(getClass());
}
