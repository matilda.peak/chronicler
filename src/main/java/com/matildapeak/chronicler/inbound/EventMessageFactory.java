/*
 * Copyright (c) 2018 Matilda Peak - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited. Proprietary and confidential.
 */
package com.matildapeak.chronicler.inbound;

import com.google.protobuf.ByteString;
import com.matildapeak.chronicler.util.SequenceIdGenerator;
import io.micrometer.core.instrument.Metrics;
import matildapeak.chronicler.EventCategoryEnumOuterClass.EventCategoryEnum;
import matildapeak.chronicler.EventMessageOuterClass.EventMessage;
import net.jcip.annotations.Immutable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Map;
import java.util.Optional;

/**
 * Creates instances of {@link EventMessage} from a HTTP request
 *
 * @author Alan Christie
 */
@Component
@Immutable
class EventMessageFactory
{

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public EventMessage make(
            final String customer,
            final String domain,
            final String group,
            final String source,
            final String type,
            final Map<String, String> header,
            final Optional<byte[]> payload)
    {
        Metrics.summary("inbound.payload.size", "customer", customer)
               .record(payload.orElse(emptyArray).length);

        return EventMessage.newBuilder()
                           .setCustomerId(customer)
                           .setUuId(eventIdGenerator.generate())
                           .setDomainId(domain)
                           .setGroupId(group)
                           .setSourceId(source)
                           .setTime(Instant.now().toEpochMilli())
                           .setType(type)
                           .setCategory(EventCategoryEnum.EVENT_CATEGORY_CUSTOMER_DATA)
                           .putAllHeader(header)
                           .setPayload(ByteString.copyFrom(payload.orElse(emptyArray)))
                           .build();
    }

    @Autowired
    private SequenceIdGenerator eventIdGenerator;

    private final byte[] emptyArray = new byte[]{};
}
