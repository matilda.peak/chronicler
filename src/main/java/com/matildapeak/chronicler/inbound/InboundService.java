/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.inbound;

import com.matildapeak.chronicler.storage.EventLogWriter;
import matildapeak.chronicler.EventMessageOuterClass.EventMessage;
import net.jcip.annotations.ThreadSafe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@ThreadSafe
class InboundService
{
    public void push(final EventMessage item)
    {
        writer.append(item);
    }

    @Autowired
    private EventLogWriter writer;
}
