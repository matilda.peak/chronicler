/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler;

import io.micrometer.core.annotation.Timed;
import io.micrometer.core.aop.TimedAspect;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class MetricsConfiguration
{
    /**
     * Configures the default tags for {@code Micrometer}
     *
     * @return the customiser for Micrometer with the common tags
     */
    @Bean
    MeterRegistryCustomizer<MeterRegistry> metricsCommonTags(final String chroniclerUUID)
    {
        return registry -> registry.config().commonTags(
                "application", "chronicler",
                "uuid", chroniclerUUID);
    }

    /**
     * Enables usage of {@link Timed} annotation on arbitrary methods. Not auto-configured in Spring 2.0.2
     */
    @Bean
    public TimedAspect timedAspect(final MeterRegistry registry)
    {
        return new TimedAspect(registry);
    }
}
