/*
 * Copyright (c) 2018 Matilda Peak - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.domain;
import matildapeak.chronicler.EventMessageOuterClass.EventMessage;
import net.jcip.annotations.Immutable;
import org.springframework.util.Assert;

/**
 * This class defines the filtering criteria for a subscription.
 * Should be extended to built the filter object from the {@code criteria} definition.
 */
@Immutable
public class SubscriptionCriteria
{
    public SubscriptionCriteria(final String criteria)
    {
        Assert.notNull(criteria, "Cannot process a null criteria");
        this.criteria = criteria;

        this.fromEnd = criteria.contains("FROM END");

        // Extracted from the criteria
        this.customer = ""; // TODO
    }

    public boolean fromEnd()
    {
        return fromEnd;
    }

    public String getCustomer()
    {
        return customer;
    }

    public String getCriteria()
    {
        return criteria;
    }


    public boolean isStart(final EventMessage eventMessage)
    {
        return true;
    }

    public boolean isEnd(final EventMessage eventMessage)
    {
        return false;
    }

    public boolean accept(final EventMessage eventMessage)
    {
        return true;
    }

    @Override
    public boolean equals(final Object obj)
    {
        return criteria.equals(obj);
    }

    @Override
    public int hashCode()
    {
        return criteria.hashCode();
    }

    @Override
    public String toString()
    {
        return "SubscriptionCriteria-{" +
                "customer = '" + customer + '\'' +
                ", criteria = '" + criteria + '\'' +
                '}';
    }

    private final boolean fromEnd;

    private final String customer;
    private final String criteria;
}
