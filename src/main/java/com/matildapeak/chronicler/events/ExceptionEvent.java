/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler.events;

import java.time.Instant;

/**
 * An internal event to flag exceptions
 */
public class ExceptionEvent implements InternalEvent
{

    public ExceptionEvent(final String reason, final Throwable cause)
    {
        this.timestamp = Instant.now();
        this.reason = reason;
        this.cause = cause;
    }

    public Instant getTimestamp()
    {
        return timestamp;
    }

    public String getReason()
    {
        return reason;
    }

    public Throwable getCause()
    {
        return cause;
    }

    private final Instant timestamp;

    private final String reason;

    private final Throwable cause;
}