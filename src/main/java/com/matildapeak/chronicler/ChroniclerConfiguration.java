/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler;

import com.google.common.eventbus.EventBus;
import com.matildapeak.chronicler.util.SequenceIdGenerator;
import com.matildapeak.chronicler.util.UUIDGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ChroniclerConfiguration
{
    /**
     * TODO: create a listener that injects this events back in chronicler's event log
     *
     * @return and bus for internal events.
     */
    @Bean
    public EventBus internalEventBus()
    {
        return new EventBus();
    }

    /**
     * The {@code Chronicler instance UUID}
     *
     * @return a base64 representation of an edited value of a UUID version 4 plus ended by a {@code #}' separator
     */
    @Bean
    public String chroniclerUUID()
    {
        return new UUIDGenerator().makeId() + '#';
    }

    @Bean
    public SequenceIdGenerator eventIdGenerator()
    {
        return new SequenceIdGenerator(chroniclerUUID());
    }
}
