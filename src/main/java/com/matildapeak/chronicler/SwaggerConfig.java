/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler;

import com.fasterxml.classmate.TypeResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.WildcardType;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.ServletContext;
import java.net.URI;
import java.util.ArrayList;
import java.util.Optional;
import java.util.concurrent.Callable;

import static springfox.documentation.schema.AlternateTypeRules.newRule;

@Configuration
@EnableSwagger2
public class SwaggerConfig
{
    /**
     * Configures {@code Springfox} to produce the desired Swagger documentation.
     * <p>
     * Access swagger via the <a href="http://chronicler:8080/v2/api-docs">api</a>
     * or the <a href="http://chronicler:‌‌‌8080/swagger-ui.html">ui</a>
     *
     * @see <a href="http://springfox.github.io/springfox/docs/current/">Sprinfox documentation</a>
     */
    @Bean
    public Docket api(final ServletContext servletContext)
    {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage(getClass().getPackage().getName()))
                .paths(PathSelectors.any())
                .build()
                .directModelSubstitute(URI.class, String.class)
                .genericModelSubstitutes(Optional.class, ResponseEntity.class)
                .alternateTypeRules(
                        newRule(typeResolver.resolve(Callable.class, typeResolver.resolve(ResponseEntity.class, WildcardType.class)),
                                typeResolver.resolve(WildcardType.class))) // Callable<ResponseEntity<T>> to 'T'
                .apiInfo(new ApiInfo(
                        "Chronicler REST API",
                        "Use the 'inbound' interface for pushing events into Chronicler " +
                                "and the 'outbound' interface to open websocket channels to read events from it.",
                        "2018.4",
                        "urn:tos", // TODO: define
                        new Contact("MatildaPeak", "http://www.matildapeak.com", "info@matildapeak.com"),
                        "Copyright (C) 2018 Matilda Peak - All Rights Reserved",
                        "",
                        new ArrayList<>()));
    }

    @Autowired
    private TypeResolver typeResolver;
}
