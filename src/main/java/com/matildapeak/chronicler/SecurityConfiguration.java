/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler;

import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter
{
    public static final String ROLE_ACTUATOR = "ACTUATOR";

    @Override
    protected void configure(final HttpSecurity httpSecurity) throws Exception
    {
        // https://github.com/spring-projects/spring-boot/wiki/Spring-Boot-Security-2.0#custom-security-example

        httpSecurity.authorizeRequests()
                    .requestMatchers(EndpointRequest.to("health", "info", "prometheus")).permitAll()
                    .requestMatchers(EndpointRequest.toAnyEndpoint()).hasRole(ROLE_ACTUATOR)
                    .antMatchers("/swagger/**").permitAll()
                    .antMatchers("/inbound/**", "/outbound/**", "/channel/**").permitAll()
                    .anyRequest().denyAll()
                    .and()
                    // https://docs.spring.io/spring-security/site/docs/current/reference/html/csrf.html#when-to-use-csrf-protection
                    .csrf().disable()
// TODO: add exception handling
//                    .exceptionHandling()
//                    .authenticationEntryPoint(unauthorizedHandler)
//                    .accessDeniedHandler(accessDeniedHandler)
                    .httpBasic();
    }

    @Override
    public void configure(final WebSecurity web)
    {
        // Make sure we give full access to Swagger UI and API
        web.ignoring()
           .antMatchers(
                   "/v2/api-docs",
                   "/swagger-resources/**",
                   "/swagger-ui.html**",
                   "/webjars/**",
                   "favicon.ico");
    }
}
