/*
 * Copyright (C) 2018 Matilda Peak - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

package com.matildapeak.chronicler;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Customises the MVC configuration created by boot auto-configuration.
 * DO NOT use @EnableWebMvc as we want to keep using Spring Boot
 * MVC features
 * https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-developing-web-applications.html#boot-features-spring-mvc-auto-configuration
 */
@Configuration
public class WebConfiguration implements WebMvcConfigurer
{
    @Override
    public void configureAsyncSupport(final AsyncSupportConfigurer configurer)
    {
        configurer.setTaskExecutor(asyncTaskExecutor);
    }

    /**
     * Creates the {@link AsyncTaskExecutor} to use by {@code Spring MVC DispatcherServlet} to server asynchronous
     * requests
     */
    @Bean
    @Lazy
    public AsyncTaskExecutor asyncTaskExecutor(
            @Value("${com.matildapeak.chronicler.web.corepoolsize:10}") int corePoolSize,
            @Value("${com.matildapeak.chronicler.web.maxpoolsize:100}") int maxPoolSize)
    {
        logger.info("Creating async task executor");

        final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(corePoolSize);
        executor.setMaxPoolSize(maxPoolSize);
        executor.setThreadFactory(new ThreadFactoryBuilder().setNameFormat("AsyncTaskExecutor-%s").build());

        logger.debug("AsyncTaskExecutor = {}", executor);
        return executor;
    }

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private AsyncTaskExecutor asyncTaskExecutor;
}
