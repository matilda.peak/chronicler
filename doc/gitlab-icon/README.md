The gitlab icon comes from https://www.iconspng.com/image/92795/icon-book
where the licencing is: -

>   Licencing! Icon book PNG icons - The pictures are free for personal and
    even for commercial use. You can modify, copy and distribute the vectors on
    Icon book in iconspng.com. All without asking for permission or setting a
    link to the source. So, attribution is not required.
    