---
Title:      Instructions for locally testing the application  
Author:     Alan Christie
Date:       26 November 2017
Copyright:  Matilda Peak Limited. All rights reserved.
---

## Building the application

First export the version you want for the solution (can be anything you want):

```bash
export SOLUTION_VERSION=2018.3
```

And then build it with _gradle_:

```bash
./gradlew build
```

## Launching the application

Launch the application in the same terminal window by executing:

```bash
./runlocal.sh
```

The application will start and wait there listening for request.
(Make sure you take some time to contemplate the logo in the banner)

The script defines a testing password for the support user `matildapeak`, set to
`say friend and enter` encoding with `bcrypt`

## Testing the application

In another terminal execute the following commands:

To check the health status as performed by Openshift, execute

```bash
curl -L http://localhost:8080/actuator/health
```

Some basic building information can be gathered with

```bash
curl -L http://localhost:8080/actuator/info
```

To list the _env_, _threaddump_, _metrics_ or _loggers_, you must
pass on the _username_ and _password_ as those actuators are protected:

```bash
curl -u matildapeak:"say friend and enter" -L http://localhost:8080/actuator/env
```

## Swagger

Access the API in _json_ format by executing

```bash
curl -L http://localhost:8080/v2/api-docs
```

The `UI` can be accessed by hitting the address http://localhost:8080/swagger-ui.html
from a web browser.
