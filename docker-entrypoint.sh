#!/bin/bash

# ----------------------------------------------------------------------------
# Copyright (C) 2018 Matilda Peak Limited - All rights reserved.
# Unauthorized copying of this file, via any medium is strictly prohibited.
# Proprietary and confidential.
# ----------------------------------------------------------------------------

now=`date '+%Y%m%d-%H%M%S'`

# ----------------------------------------------------------------------------
# Superuser? No, thank you.
# ----------------------------------------------------------------------------
if [[ $EUID -eq 0 ]]; then
    echo "This application is not meant to be run by the superuser" 1>&2
    exit 1
fi

# ----------------------------------------------------------------------------
# JAVA DEFINITIONS
# ----------------------------------------------------------------------------

JAVA_MAX_MEMORY=${JAVA_MAX_MEMORY-2048m}

# The options we must always run the server with
JVM_JAVA_OPTIONS="-server \
 -Xms$JAVA_MAX_MEMORY \
 -Xmx$JAVA_MAX_MEMORY \
 -XX:+UseG1GC \
 -XX:MaxGCPauseMillis=200 \
 -XX:ParallelGCThreads=20 \
 -XX:ConcGCThreads=5 \
 -XX:InitiatingHeapOccupancyPercent=70 \
 -XX:+UseStringDeduplication"

# ----------------------------------------------------------------------------
# EXECUTION
# using 'exec' because of
# http://veithen.github.io/2014/11/16/sigterm-propagation.html
# ----------------------------------------------------------------------------

# Change to workdir
cd /opt/chronicler

# Launch it
exec $JAVA_HOME/bin/java $JVM_JAVA_OPTIONS $JAVA_OPTS -jar chronicler.jar --spring.profiles.active=production,${CHRONICLER_STORAGE_PROFILE-kafka}
