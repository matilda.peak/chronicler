---
Title:      The Matilda Peak chronicler application
Author:     Alan Christie
Date:       4 August 2018
Copyright:  Matilda Peak Limited. All rights reserved.
---

[![build status](https://gitlab.com/matilda.peak/chronicler/badges/master/build.svg)](https://gitlab.com/matilda.peak/chronicler/commits/master)

# Chronicler
A topic-based message storage and retrieval service.
