#!/bin/bash

RED='\033[0;31m'
NC='\033[0m'    # No Color
echo -e "${RED}This script is only meant for testing while building${NC}"

# "say friend and enter"
export CHRONICLER_USER_PASSWORD="\$2a\$10\$fxu2VjXF4FViPZeKYH16xemkPwI.wLZai6fixM7xCQCnBxAcuhsUG"

JAVA_MAX_MEMORY=${JAVA_MAX_MEMORY-2048m}

JVM_JAVA_OPTIONS="-server \
 -Xms$JAVA_MAX_MEMORY \
 -Xmx$JAVA_MAX_MEMORY \
 -XX:+UseG1GC \
 -XX:MaxGCPauseMillis=200 \
 -XX:ParallelGCThreads=20 \
 -XX:ConcGCThreads=5 \
 -XX:InitiatingHeapOccupancyPercent=70 \
 -XX:+UseStringDeduplication"

exec java $JVM_JAVA_OPTIONS -jar build/libs/chronicler-*.jar --spring.profiles.active=production,queue
